package com.hospital.book;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.hospital.book.DI.DaggerComponent;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.UI.Adapter.ContactAdapter;
import com.hospital.book.Utils.Permission;
import com.hospital.book.databinding.ContactlistdialogBinding;
import com.hospital.book.databinding.DonoruserdetailsBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DonorUserDetails extends AppCompatActivity {

    private DonoruserdetailsBinding binding;
    private DonorModel donorModel;
    @Inject
    ContactAdapter contactAdapter;
    private List<ContactModel> contactModelList = new ArrayList<>();
    private ViewModel viewModel;
    private FirebaseAuth Mauth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.donoruserdetails);


        donorModel = (DonorModel)getIntent().getSerializableExtra(DataManager.Data);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        Mauth = FirebaseAuth.getInstance();

        var component = DaggerComponent.create();
        component.InjectDonorUserDetails(this);

        binding.setDonor(donorModel);
        InitView();
        SetContact();
        GetProfileImage();

        var FirebaseUser = Mauth.getCurrentUser();
        if(!FirebaseUser.getUid().equals(donorModel.getSenderUID())){
            binding.ChatNowText.setVisibility(View.VISIBLE);
            binding.ChatIcon.setVisibility(View.VISIBLE);
            binding.ChatBtn.setVisibility(View.VISIBLE);
        }
    }

    private void GetProfileImage(){
        viewModel.GetUserInfo(donorModel.getSenderUID()).observe(this, profileModel -> {
            if(profileModel != null){
                Picasso.get().load(profileModel.getProfileImage()).into(binding.ProfileImage);
            }
        });
    }

    private void SetContact(){
        if(donorModel.getMainPhoneNumber() != null){
            var MainNumber = new ContactModel("Main Number", donorModel.getMainPhoneNumber());
            contactModelList.add(MainNumber);
        }
        if(donorModel.getRoshanNumber() != null){
            var RoshanNumber = new ContactModel(DataManager.RoshanNumber, donorModel.getRoshanNumber());
            contactModelList.add(RoshanNumber);
        }
        if(donorModel.getEtisalatNumber() != null){
            var EtisalatNumber = new ContactModel(DataManager.EtisalatNumber, donorModel.getEtisalatNumber());
            contactModelList.add(EtisalatNumber);
        }
        if(donorModel.getAWCCNumber() != null){
            var AWCC = new ContactModel(DataManager.AWCCNumber, donorModel.getAWCCNumber());
            contactModelList.add(AWCC);
        }
        if(donorModel.getMTNNumber() != null){
            var MTNNumber = new ContactModel(DataManager.MTNNumber, donorModel.getMTNNumber());
            contactModelList.add(MTNNumber);
        }
        if(donorModel.getWhatsappNumber() != null){
            var whatsappnumber = new ContactModel(DataManager.WhatsappNumber, donorModel.getWhatsappNumber());
            contactModelList.add(whatsappnumber);
        }

    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
        });
        binding.Toolbar.Title.setText(donorModel.getGivenName());

        binding.CallBtn.setOnClickListener(view -> {
            var dialog = new AlertDialog.Builder(DonorUserDetails.this);
            ContactlistdialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.contactlistdialog, null, false);
            dialog.setView(v.getRoot());


            AlertDialog alertDialog = dialog.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();

            v.ContactRecyclerView.setHasFixedSize(true);
            contactAdapter.setList(contactModelList);
            v.ContactRecyclerView.setAdapter(contactAdapter);

            v.ContinueBtn.setOnClickListener(view1 -> {
                alertDialog.dismiss();
            });
        });

        contactAdapter.OnCLickLisiner((Number, Type) -> {
            var messagetext = "";
            if(Type.equals(DataManager.Call)){
                if(Permission.PermissionCall(DonorUserDetails.this, 544)){
                    var intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+Number));
                    startActivity(intent);
                }

                if(Type.equals(DataManager.Message)){
                    if(Permission.PermissionSendMessage(DonorUserDetails.this, 200)){
                        var smsmanager = SmsManager.getDefault();
                        // smsmanager.sendTextMessage(Number, );
                    }
                }

            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }
}