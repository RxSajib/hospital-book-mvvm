package com.hospital.book.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.hospital.book.BloodBankModel;
import com.hospital.book.BloodRequestModel;
import com.hospital.book.Data.DataManager;
import com.hospital.book.DonorModel;
import com.hospital.book.DonorUserDetails;
import com.hospital.book.Home;
import com.hospital.book.RegisterDonationModel;
import com.hospital.book.UI.AddBloodToBank;
import com.hospital.book.UI.AddRegisterAsFriend;
import com.hospital.book.UI.AddRegisterDonation;
import com.hospital.book.UI.BloodBank;
import com.hospital.book.UI.BloodDonationHistoryDetails;
import com.hospital.book.UI.BloodDonors;
import com.hospital.book.UI.BloodRequest.RequestOFBloodHome;
import com.hospital.book.UI.DetailsOFBloodRequest;
import com.hospital.book.UI.DetailsOFRegisterBloodDonor;
import com.hospital.book.UI.EditBloodBank;
import com.hospital.book.UI.FindDonorFilter;
import com.hospital.book.UI.FindDonorsResult;
import com.hospital.book.UI.ForgotPassword;
import com.hospital.book.UI.LastDonationHistory;
import com.hospital.book.UI.Profile;
import com.hospital.book.UI.RegisterAsFriend;
import com.hospital.book.UI.SignIn;
import com.hospital.book.UI.SplashScreen;
import com.hospital.book.UI.TelephoneDirectory;

public class HandleActivity {

    public static void GotoSplashScreen(Context context){
        var intent = new Intent(context, SplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
    public static void AddRegisterDonation(Context context){
        var intent = new Intent(context, AddRegisterDonation.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
    public static void GotoAddRegisterAsFriend(Context context){
        var intent = new Intent(context, AddRegisterAsFriend.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void BloodDonationHistoryDetails(Context context, RegisterDonationModel registerDonationModel){
        var intent = new Intent(context, BloodDonationHistoryDetails.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, registerDonationModel);
        context.startActivity(intent);
    }

    public static void GotoLastDonationHistory(Context context, String UID){
        var intent = new Intent(context, LastDonationHistory.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.UID, UID);
        context.startActivity(intent);
    }
    public static void GotoRegisterAsFriend(Context context, DonorModel model) {
        var intent = new Intent(context, RegisterAsFriend.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, model);
        context.startActivity(intent);
    }
    public static void GotoSplashScreen(Activity activity){
        var intent = new Intent(activity, SplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static void GotoDetailsOFRegisterBloodDonor(Context context, DonorModel model) {
        var intent = new Intent(context, DetailsOFRegisterBloodDonor.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, model);
        context.startActivity(intent);
    }

    public static void GotoBloodDonor(Activity activity){
        var intent = new Intent(activity, BloodDonors.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static void GotoFindDonorsResult(Activity activity, String BloodGroup, String CountryName, String ProvinceName, String District){
        var intent = new Intent(activity, FindDonorsResult.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.BloodGroup, BloodGroup);
        intent.putExtra(DataManager.CountryName, CountryName);
        intent.putExtra(DataManager.Province, ProvinceName);
        intent.putExtra(DataManager.District, District);
        activity.startActivity(intent);
    }

    public static void GotoBloodRequestDetails(Context context, BloodRequestModel model) {
        var intent = new Intent(context, DetailsOFBloodRequest.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, model);
        context.startActivity(intent);
    }
    public static void GotoFindDonorFilter(Activity activity){
        var intent = new Intent(activity, FindDonorFilter.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static void GotoRequestOFBloodHome(Activity activity){
        var intent = new Intent(activity, RequestOFBloodHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static void GotoBloodBank(Context context){
        var intent = new Intent(context, BloodBank.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBloodResult(Context context, DonorModel donorModel){
        var intent = new Intent(context, DonorUserDetails.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, donorModel);
        context.startActivity(intent);
    }

    public static void GotoEditBloodBank(Context context, BloodBankModel bloodBankModel){
        var intent = new Intent(context, EditBloodBank.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, bloodBankModel);
        context.startActivity(intent);
    }

    public static void GotoAddBloodToBank(Context context){
        var intent = new Intent(context, AddBloodToBank.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoSignIn(Activity activity){
        var intent = new Intent(activity, SignIn.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static void GotoHome(Activity activity){
        var intent = new Intent(activity, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static void GotoForgotPassword(Context context){
        var intent = new Intent(context, ForgotPassword.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoTelephoneDirectory(Context context){
        var intent = new Intent(context, TelephoneDirectory.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoProfile(Context context){
        var intent = new Intent(context, Profile.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
}
