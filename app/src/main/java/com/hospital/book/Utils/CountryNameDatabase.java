package com.hospital.book.Utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.hospital.book.CountryNameModel;
import com.hospital.book.DistrictNameModel;
import com.hospital.book.OrganizationTypeModel;
import com.hospital.book.ProvinceNameModel;

@Database(entities = {CountryNameModel.class, ProvinceNameModel.class, DistrictNameModel.class, OrganizationTypeModel.class}, version = 1)
public abstract class CountryNameDatabase extends RoomDatabase {

    public abstract LocationDao countryDao();

    public static volatile CountryNameDatabase getInstance;

    public static CountryNameDatabase GetContactDatabase(Context context) {
        if (getInstance == null) {
            synchronized (CountryNameDatabase.class) {
                if (getInstance == null) {
                    getInstance = Room.databaseBuilder(context, CountryNameDatabase.class, "contact")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return getInstance;
    }
}
