package com.hospital.book;

import lombok.Data;

@Data
public class User {
    private long DocumentKey, Timestamp;
    private String Email, HospitalLogo, HospitalName, Location, UID;
}
