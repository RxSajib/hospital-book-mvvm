package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.nex3z.togglebuttongroup.button.CircularToggle;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.Utils.Toast;
import com.hospital.book.Widget.DatePickerDialogFragment;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.databinding.AddbloodtobankBinding;

import javax.inject.Inject;

public class AddBloodToBank extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private AddbloodtobankBinding binding;
    private String[] BloodCategory = {"Whole Blood (Refrigerated Full Blood)", "Single Donor Platelet", "Single Donor Plasma", "Sagma Packed Red Blood Cells", "Platelet Poor Plasma",
            "Platelete Concentrate", "Plasma", "Packed Red Blood Cells", "Leukoreduced RBC", "Irradiated RBC", "Fresh Frozen Plasma", "Packed Red Blood Cells",
            "Leukoreduced RBC", "Irradiated RBC", "Fresh Frozen Plasma", "Cryoprecipitate", "Croyo Poor Plasma"};

    private String[] Status = {"Available", "Donated", "Expired"};

    private String BloodCategoryItem = null;
    private String StatusItem = null;
    private ViewModel viewModel;
    private CircularToggle circularToggle;
    private String BloodGroup = null;
    @Inject
    ProgressDialog progressDialog;
    @Inject
    public DatePickerDialogFragment datePickerDialogFragment;
    private boolean BloodDonationDate = false;
    private boolean BloodExpDate = false;
    private String HospitalLogo, HospitalName, Location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addbloodtobank);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });

        InitView();
        BloodCategorySpinner();
        UploadData();
        GetBloodCategory();
        GetProfile();
    }

    private void GetProfile(){
        viewModel.GetCurrentUser().observe(this, user -> {
            if(user != null) {
                HospitalLogo = user.getHospitalLogo();
                HospitalName = user.getHospitalName();
                Location = user.getLocation();
            }
        });
    }

    private void GetBloodCategory() {
        binding.SelectBloodCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                BloodCategoryItem = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.StatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                StatusItem = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void UploadData() {
        binding.UploadBtn.setOnClickListener(view -> {

            var TagNumber = binding.TagNumberInput.getText().toString().trim();
            var Quantity = binding.QuantityInput.getText().toString().trim();
            var DonorName = binding.DonorNameInput.getText().toString().trim();
            var DonorPhoneNumber = binding.DonorPhoneNumberInput.getText().toString().trim();
            var BloodDonationDate = binding.BloodDonationDateInput.getText().toString().trim();
            var BloodExpiryDate = binding.BloodExpiryDateInput.getText().toString().trim();


            if (BloodCategoryItem == null) {
                Toast.Message(AddBloodToBank.this, "Select blood category");
            } else if (BloodGroup == null) {
                Toast.Message(AddBloodToBank.this, "Blood group require");
            } else if (Quantity.isEmpty()) {
                Toast.Message(AddBloodToBank.this, "Blood quantity require");
            } else if (BloodDonationDate.isEmpty()) {
                Toast.Message(AddBloodToBank.this, "Blood Donation date require");
            } else if (BloodExpiryDate.isEmpty()) {
                Toast.Message(AddBloodToBank.this, "Blood Expiry date require");
            } else if (StatusItem == null) {
                Toast.Message(AddBloodToBank.this, "Select blood status");
            } else {
                var Timestamp = System.currentTimeMillis();
                viewModel.SearchBloodStorage(BloodCategoryItem, BloodGroup).observe(this, bloodBankModels -> {
                    if (bloodBankModels != null) {
                        progressDialog.ProgressDialog(AddBloodToBank.this);
                        var totalprice = Integer.valueOf(bloodBankModels.get(0).getQuantity()) + Integer.valueOf(Quantity);
                        viewModel.UpdateBloodStoragePost(bloodBankModels.get(0).getDocumentID(), String.valueOf(totalprice)).observe(this, aBoolean -> {
                            if (aBoolean) {
                                finish();
                                Toast.Message(getApplicationContext(), "Add success");
                                progressDialog.CancelProgressDialog();
                            } else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
                    } else {
                        progressDialog.ProgressDialog(AddBloodToBank.this);
                        viewModel.StorageBloodPOST(HospitalLogo, HospitalName, Location, Timestamp, BloodCategoryItem, BloodGroup, TagNumber, Quantity, DonorName, DonorPhoneNumber, BloodDonationDate, BloodExpiryDate, StatusItem)
                                .observe(this, aBoolean -> {
                                    if (aBoolean) {
                                        viewModel.UploadBlood(HospitalLogo, HospitalName, Location, Timestamp, BloodCategoryItem, BloodGroup, TagNumber, Quantity, DonorName, DonorPhoneNumber, BloodDonationDate, BloodExpiryDate, StatusItem)
                                                .observe(this, aBoolean1 -> {
                                                    if (aBoolean1) {
                                                        finish();
                                                        Toast.Message(getApplicationContext(), "Add success");
                                                        progressDialog.CancelProgressDialog();
                                                    }
                                                });

                                    }
                                });
                    }
                });

            }
        });
    }

    private void BloodCategorySpinner() {
        var adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.bloodbankspinneritem, R.id.BloodBankTitle, BloodCategory);
        binding.SelectBloodCategorySpinner.setAdapter(adapter);

        var statusadapter = new ArrayAdapter<>(getApplicationContext(), R.layout.bloodbankspinneritem, R.id.BloodBankTitle, Status);
        binding.StatusSpinner.setAdapter(statusadapter);
    }

    private void InitView() {
        binding.BloodDonationDateInput.setKeyListener(null);
        binding.BloodExpiryDateInput.setKeyListener(null);

        binding.Toolbar.Title.setText(getResources().getString(R.string.AddBlood));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
        });

        binding.BloodDonationDateInput.setOnClickListener(view -> {
            BloodDonationDate = true;
            var dialog = new DatePickerDialogFragment();
            dialog.show(getSupportFragmentManager(), "sdsaasd");
        });

        binding.BloodExpiryDateInput.setOnClickListener(view -> {
            BloodExpDate = true;
            var dialog = new DatePickerDialogFragment();
            dialog.show(getSupportFragmentManager(), "sdsaasd");
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        if (BloodDonationDate) {
            binding.BloodDonationDateInput.setText(i + "-" + i1 + "-" + i2);
            BloodDonationDate = false;
        }
        if (BloodExpDate) {
            binding.BloodExpiryDateInput.setText(i + "-" + i1 + "-" + i2);
            BloodExpDate = false;
        }
    }
}