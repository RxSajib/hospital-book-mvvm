package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Data.DataManager;
import com.hospital.book.DonorModel;
import com.hospital.book.Network.ViewModel.LocationViewModel;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.DistrictNameDataAdapter;
import com.hospital.book.UI.Adapter.LocationDataAdapter;
import com.hospital.book.UI.Adapter.ProvinceNameDataAdapter;
import com.hospital.book.Utils.BetweenTwoDate;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.Utils.Toast;
import com.hospital.book.Widget.DatePickerDialogFragment;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.Widget.TermsAndConditionDialog;
import com.hospital.book.databinding.DetailsofregisterblooddonorBinding;
import com.hospital.book.databinding.LocationdialogBinding;
import com.nex3z.togglebuttongroup.button.CircularToggle;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DetailsOFRegisterBloodDonor extends AppCompatActivity {

    private DetailsofregisterblooddonorBinding binding;
    private DonorModel donorModel;
    private String[] RegisterDonationDate = {"3 Months", "4 Months", "5 Months", "6 Months", "7 Months", "8 Months", "9 Months", "10 Months", "11 Months", "12 Months"};
    private String SelectRadioItem = null;
    private TermsAndConditionDialog termsAndConditionDialog;
    ProgressDialog progressDialog;
    private String Month;
    private ViewModel viewModel;
    private int LastDonationTotalDay;
    private LocationDataAdapter locationDataAdapter;
    private ProvinceNameDataAdapter provinceNameDataAdapter;
    private LocationViewModel locationViewModel;
    private DistrictNameDataAdapter districtNameDataAdapter;
    private String BloodGroup = null;
    private CircularToggle circularToggle;
    private static final String TAG = "DetailsOFRegisterBloodD";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.detailsofregisterblooddonor);
        donorModel = (DonorModel) getIntent().getSerializableExtra(DataManager.Data);


        termsAndConditionDialog = new TermsAndConditionDialog();
        termsAndConditionDialog.OnClickLisiner(() -> {

        });
        donorModel = (DonorModel) getIntent().getSerializableExtra(DataManager.Data);
        binding.setBloodDonor(donorModel);
        progressDialog = new ProgressDialog();
        //todo new
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        locationDataAdapter = new LocationDataAdapter();
        provinceNameDataAdapter = new ProvinceNameDataAdapter();
        districtNameDataAdapter = new DistrictNameDataAdapter();


        InitView();
        UploadRegisterDonor();
        SetLoactionData();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        //todo new

        BloodGroupSet();

        Log.d(TAG, donorModel.getSenderUID());
    }

    private void BloodGroupSet(){
        if(donorModel.getBloodGroup().equals(DataManager.APlus)){
            binding.APlus.setChecked(true);
            BloodGroup = DataManager.APlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.AMinus)){
            binding.AMinus.setChecked(true);
            BloodGroup  = DataManager.AMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BPlus)){
            binding.BPlus.setChecked(true);
            BloodGroup = DataManager.BPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BMinus)){
            binding.BMinus.setChecked(true);
            BloodGroup = DataManager.BMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABPlus)){
            binding.ABPlus.setChecked(true);
            BloodGroup = DataManager.ABPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABMinus)){
            binding.ABMinus.setChecked(true);
            BloodGroup = DataManager.ABMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OPlus)){
            binding.OPlus.setChecked(true);
            BloodGroup = DataManager.OPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OMinus)){
            binding.OMinus.setChecked(true);
            BloodGroup = DataManager.OMinus;
        }
    }


    /*
    private void BloodGroupSet(){
        if(donorModel.getBloodGroup().equals(DataManager.APlus)){
            binding.APlus.setChecked(true);
            BloodGroup = DataManager.APlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.AMinus)){
            binding.AMinus.setChecked(true);
            BloodGroup  = DataManager.AMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BPlus)){
            binding.BPlus.setChecked(true);
            BloodGroup = DataManager.BPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BMinus)){
            binding.BMinus.setChecked(true);
            BloodGroup = DataManager.BMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABPlus)){
            binding.ABPlus.setChecked(true);
            BloodGroup = DataManager.ABPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABMinus)){
            binding.ABMinus.setChecked(true);
            BloodGroup = DataManager.ABMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OPlus)){
            binding.OPlus.setChecked(true);
            BloodGroup = DataManager.OPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OMinus)){
            binding.OMinus.setChecked(true);
            BloodGroup = DataManager.OMinus;
        }
    }


    private void SetLocation() {

        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
            });
        });

        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(DetailsOFRegisterBloodDonor.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });
    }

    private void UpdateData() {

        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });
        binding.UploadBtn.setOnClickListener(view -> {
            var GivenName = binding.GivenNamesInput.getText().toString().trim();
            var SureName = binding.SurnameInput.getText().toString().trim();
            var FatherName = binding.FatherNameInput.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Weight = binding.WeightInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreaName = binding.AreaNameInput.getText().toString().trim();
            var PhoneNuber = binding.MainNumberInput.getText().toString().trim();
            var RoshanNumber = binding.RoshanNumberInput.getText().toString().trim();
            var EtisalatNumber = binding.EtisalatNumberInput.getText().toString().trim();
            var AWCCNumber = binding.AWCCNumberInput.getText().toString().trim();
            var MTNNumber = binding.MTNNumberInput.getText().toString().trim();
            var WhatsappNumber = binding.WhatsappNumberInput.getText().toString().trim();
            var EmailAddress = binding.EmailAddressInput.getText().toString().trim();


            if (Integer.valueOf(Age) < 17) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Age must be at least 18 years");
            } else if (Integer.valueOf(Weight) < 54) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Weight must be at least 55");
            }else {
                progressDialog.ProgressDialog(DetailsOFRegisterBloodDonor.this);
                viewModel.UpdateRegisterAsDonor(donorModel.getSenderUID(), GivenName, SureName, FatherName, BloodGroup, Age, Weight, Country, Province, District, AreaName, PhoneNuber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress)
                        .observe(this, aBoolean -> {
                            if(aBoolean){
                                progressDialog.CancelProgressDialog();
                                finish();
                                Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
                            }else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
            }

        });
    }

    private void InitView() {
        binding.EditBtn.setOnClickListener(view -> {
            var intent = new Intent(this, EditregisterDonationDate.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            Animatoo.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });

        binding.DonationHistoryBtn.setOnClickListener(view -> {
            HandleActivity.GotoLastDonationHistory(this, donorModel.getSenderUID());
            Animatoo.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });
    }

    private void SetToolbar() {
        binding.Toolbar.ToolbarTitle.setText("Edit/Delete");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
    }


    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }


     */
    //todo get all location name


    //todo new
    private void SetLoactionData() {
        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
                if(countryNameModels != null){
                    locationViewModel.InsertCounyryNameData(countryNameModels);
                }
            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
            });
        });

        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(DetailsOFRegisterBloodDonor.this, provinceNameModels -> {
                    if(provinceNameModels != null){
                        locationViewModel.InsertProvincesNameData(provinceNameModels);
                    }
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
                    if(districtNameModelList != null){
                        locationViewModel.InsertDistrict(districtNameModelList);
                    }
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });

    }

    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
    //todo get all location name



    private void InitView() {

        binding.Toolbar.Title.setText("Details of donor");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(DetailsOFRegisterBloodDonor.this);
        });

        binding.DonationHistoryBtn.setOnClickListener(view -> {
            HandleActivity.GotoLastDonationHistory(this, donorModel.getSenderUID());
            Animatoo.INSTANCE.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });

        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });



        var AdapterRegisterDonate = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlayout, R.id.SpinnerTitle, RegisterDonationDate);
        binding.HowManyMonthSpinner.setAdapter(AdapterRegisterDonate);
        binding.HowManyMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                var Name = adapterView.getItemAtPosition(i).toString();
                Month = Name.replaceAll("[^0-9]", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void UploadRegisterDonor() {


        binding.UploadBtn.setOnClickListener(view -> {

            var sdf = new SimpleDateFormat(DataManager.DatePattern);
            long timestamp = System.currentTimeMillis();
            var calender = Calendar.getInstance(Locale.ENGLISH);
            calender.setTimeInMillis(timestamp);
            var currentdate = DateFormat.format(DataManager.DatePattern, calender).toString();



            var GivenName = binding.GivenNamesInput.getText().toString().trim();
            var SureName = binding.SurnameInput.getText().toString().trim();
            var FatherName = binding.FatherNameInput.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Weight = binding.WeightInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreaName = binding.AreaNameInput.getText().toString().trim();


            var MainPhonenumber = binding.MainNumberInput.getText().toString().trim();
            var RoshanPhoneNumber = binding.RoshanNumberInput.getText().toString().trim();
            var EtisalatPhoneNumber = binding.EtisalatNumberInput.getText().toString().trim();
            var AWCCPhoneNumber = binding.AWCCNumberInput.getText().toString().trim();
            var MTNPhoneNumber = binding.MTNNumberInput.getText().toString().trim();
            var WhatsappNumber = binding.WhatsappNumberInput.getText().toString().trim();
            var EmailAddress = binding.EmailAddressInput.getText().toString().trim();

            if (GivenName.isEmpty()) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Given name is empty");
            } else if (SureName.isEmpty()) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Sure name is empty");
            } else if (FatherName.isEmpty()) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Father name is empty");
            } else if (BloodGroup == null) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Blood group is empty");
            } else if (Age.isEmpty()) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Age is empty");
            } else if (Weight.isEmpty()) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Weight is empty");
            } else if (Country == "") {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Country is empty");
            } else if (Province == "") {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Province is empty");
            } else if (District == "") {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "District is empty");
            } else if (AreaName.isEmpty()) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Area name is empty");
            } else if (Integer.valueOf(Age) < 17) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Age must be at least 18 years");
            } else if (Integer.valueOf(Weight) < 54) {
                Toast.Message(DetailsOFRegisterBloodDonor.this, "Weight must be at least 55");
            } else {

                UploadData(GivenName, SureName, FatherName, BloodGroup, Age, Weight, Country, Province, District, AreaName,
                        MainPhonenumber, RoshanPhoneNumber, EtisalatPhoneNumber, AWCCPhoneNumber, MTNPhoneNumber, WhatsappNumber,
                        EmailAddress, Month);

            }


        });
    }

    private void UploadData(String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                            String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                            String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                            String EmailAddress, String Month) {

        progressDialog.ProgressDialog(DetailsOFRegisterBloodDonor.this);

        //todo update it

        viewModel.UpdateRegisterAsDonor(donorModel.getSenderUID(), GivenName, SureName, FatherName, BloodGroup, Age, Width, Country, Province, District, AreaName, MainPhoneNumber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, Month)
                .observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                        finish();
                        Animatoo.INSTANCE.animateSlideRight(DetailsOFRegisterBloodDonor.this);
                        Toast.Message(getApplicationContext(), "Update success");
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });

    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(DetailsOFRegisterBloodDonor.this);
    }

    //todo new

}