package com.hospital.book.UI.BloodBankFragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.StorageAdapter;
import com.hospital.book.databinding.StorageBinding;

import javax.inject.Inject;

public class Storage extends Fragment {

    private StorageBinding binding;
    private ViewModel viewModel;
    @Inject
    StorageAdapter storageAdapter;

    public Storage() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.storage, container,false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        storageAdapter = new StorageAdapter();

        GetData();
        return binding.getRoot();
    }

    private void GetData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(storageAdapter);
        viewModel.GetBloodBank().observe(getActivity(), bloodBankModels -> {
            storageAdapter.setList(bloodBankModels);
            storageAdapter.notifyDataSetChanged();

            if(bloodBankModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }
}