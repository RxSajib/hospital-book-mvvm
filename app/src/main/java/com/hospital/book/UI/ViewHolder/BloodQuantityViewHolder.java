package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.databinding.BloodquantityitemBinding;

public class BloodQuantityViewHolder extends RecyclerView.ViewHolder{

    public BloodquantityitemBinding binding;

    public BloodQuantityViewHolder(@NonNull BloodquantityitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
