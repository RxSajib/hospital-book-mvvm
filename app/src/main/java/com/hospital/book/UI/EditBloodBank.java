package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.hospital.book.BloodBankModel;
import com.hospital.book.DI.DaggerComponent;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.databinding.EditbloodbankBinding;

import javax.inject.Inject;

public class EditBloodBank extends AppCompatActivity {

    private EditbloodbankBinding binding;
    private BloodBankModel bloodBankModel;
    private String[] BloodCategory = {"Whole Blood (Refrigerated Full Blood)", "Single Donor Platelet", "Single Donor Plasma", "Sagma Packed Red Blood Cells", "Platelet Poor Plasma",
            "Platelete Concentrate", "Plasma", "Packed Red Blood Cells", "Leukoreduced RBC", "Irradiated RBC", "Fresh Frozen Plasma", "Packed Red Blood Cells",
            "Leukoreduced RBC", "Irradiated RBC", "Fresh Frozen Plasma", "Cryoprecipitate", "Croyo Poor Plasma"};

    private String[] Status = {"Available", "Donated", "Expired"};
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.editbloodbank);
        bloodBankModel = (BloodBankModel)getIntent().getSerializableExtra(DataManager.Data);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = DaggerComponent.create();
        component.InjectEditBloodBank(this);

        SetSpinner();
        InitView();
        SetData();
    }

    private void SetSpinner(){
        var adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.bloodbankspinneritem, R.id.BloodBankTitle, BloodCategory);
        binding.SelectBloodCategorySpinner.setAdapter(adapter);

        var statusadapter = new ArrayAdapter<>(getApplicationContext(), R.layout.bloodbankspinneritem, R.id.BloodBankTitle, Status);
        binding.StatusSpinner.setAdapter(statusadapter);
    }

    private void SetData(){
        binding.TagNumberInput.setText(bloodBankModel.getTagNumber());
        binding.QuantityInput.setText(bloodBankModel.getQuantity());
        binding.DonorNameInput.setText(bloodBankModel.getDonorName());
        binding.DonorPhoneNumberInput.setText(bloodBankModel.getDonorPhoneNumber());
        binding.BloodDonationDateInput.setText(bloodBankModel.getDonationDate());
        binding.BloodExpiryDateInput.setText(bloodBankModel.getExpiryDate());
        binding.APlus.setChecked(true);
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.EditBlood));

        binding.DeleteBtn.setOnClickListener(view -> {
            progressDialog.ProgressDialog(EditBloodBank.this);
            viewModel.DeleteBloodBank(bloodBankModel.getDocumentID()).observe(this, aBoolean -> {
                if(aBoolean){
                    progressDialog.CancelProgressDialog();
                    finish();
                }
            });
        });
    }
}