package com.hospital.book.UI;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.BloodBankFragment.Details;
import com.hospital.book.UI.BloodBankFragment.Status;
import com.hospital.book.UI.BloodBankFragment.Storage;
import com.hospital.book.Utils.BloodPagerAdapter;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.databinding.BloodbankBinding;

public class BloodBank extends AppCompatActivity {

    private BloodbankBinding binding;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.bloodbank);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        InitView();
    }

    private void InitView() {
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(BloodBank.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.BloodBank));


        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var f = new BloodPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        f.AddFragment(new Storage(), DataManager.Storage);
        f.AddFragment(new Details(), DataManager.Details);
        f.AddFragment(new Status(), DataManager.Status);
        binding.ViewPager.setAdapter(f);

        binding.AddBtn.setOnClickListener(view -> {
            HandleActivity.GotoAddBloodToBank(BloodBank.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(BloodBank.this);
    }
}