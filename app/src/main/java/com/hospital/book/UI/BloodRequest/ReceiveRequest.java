package com.hospital.book.UI.BloodRequest;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.BloodRequestAdapter;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.databinding.ReceiverequestBinding;

public class ReceiveRequest extends Fragment {

    private ReceiverequestBinding binding;
    private ViewModel viewModel;
    private BloodRequestAdapter bloodRequestAdapter;

    public ReceiveRequest() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.receiverequest, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        bloodRequestAdapter = new BloodRequestAdapter();

        InitView();
        GetBloodRequest();
        return binding.getRoot();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(bloodRequestAdapter);

        bloodRequestAdapter.OnClickLisiner(model -> {
            HandleActivity.GotoBloodRequestDetails(getActivity(), model);
            Animatoo.INSTANCE.animateSlideLeft(getActivity());
        });
    }

    private void GetBloodRequest() {
        viewModel.GetBloodRequest().observe(getActivity(), bloodRequestModels -> {
            bloodRequestAdapter.setList(bloodRequestModels);
            bloodRequestAdapter.notifyDataSetChanged();
            if (bloodRequestModels == null) {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.message.setVisibility(View.VISIBLE);
            } else {
                binding.Icon.setVisibility(View.GONE);
                binding.message.setVisibility(View.GONE);
            }
        });

    }
}