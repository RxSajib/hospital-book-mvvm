package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.hospital.book.DI.DaggerComponent;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.DonorUserAdapter;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.databinding.FinddonorsresultBinding;

import javax.inject.Inject;

public class FindDonorsResult extends AppCompatActivity {

    private FinddonorsresultBinding binding;
    private ViewModel viewModel;
    @Inject
    DonorUserAdapter adapter;
    private String BloodGroup, CountryName, Province, District;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.finddonorsresult);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        BloodGroup = getIntent().getStringExtra(DataManager.BloodGroup);
        CountryName = getIntent().getStringExtra(DataManager.CountryName);
        Province = getIntent().getStringExtra(DataManager.Province);
        District = getIntent().getStringExtra(DataManager.District);

        var component = DaggerComponent.create();
        component.InjectFindDonorsResult(this);

        InitView();
        GetData();
    }

    private void InitView(){
        binding.Toolbar.Title.setText(BloodGroup+" Search Result");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
        });
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(adapter);

        adapter.OnClickLisiner(model -> {
            HandleActivity.GotoBloodResult(FindDonorsResult.this, model);
        });

    }


    private void GetData(){
        viewModel.SearchBloodGroup(BloodGroup, CountryName, Province, District).observe(this, bloodRequestModels -> {
            adapter.setList(bloodRequestModels);
            adapter.notifyDataSetChanged();
            if(bloodRequestModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
       // Animatoo.animateSlideRight(BloodSearchResult.this);
    }
}