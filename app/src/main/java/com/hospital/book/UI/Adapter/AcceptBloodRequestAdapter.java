package com.hospital.book.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.hospital.book.AcceptBloodRequestModel;
import com.hospital.book.UI.ViewHolder.AcceptBloodRequestViewHolder;
import com.hospital.book.databinding.AcceptbloodrequestitemBinding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class AcceptBloodRequestAdapter extends RecyclerView.Adapter<AcceptBloodRequestViewHolder>{

    @Setter
    @Getter
    private List<AcceptBloodRequestModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public AcceptBloodRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        var l = LayoutInflater.from(parent.getContext());
        var v = AcceptbloodrequestitemBinding.inflate(l, parent, false);
        return new AcceptBloodRequestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AcceptBloodRequestViewHolder holder, int position) {
        holder.binding.setAcceptBloodRequest(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(AcceptBloodRequestModel bloodRequestModel);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
