package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.R;
import com.hospital.book.databinding.EditregisterdonationdateBinding;

public class EditregisterDonationDate extends AppCompatActivity {

    private EditregisterdonationdateBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.editregisterdonationdate);

        InitView();
    }

    private void InitView(){
        binding.Toolbar.Title.setText("Register Donation Date Edit");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(EditregisterDonationDate.this);
        });


    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(EditregisterDonationDate.this);
    }
}