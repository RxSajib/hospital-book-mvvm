package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.os.Handler;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.R;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.databinding.SplashscreenBinding;

public class SplashScreen extends AppCompatActivity {

    private SplashscreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.splashscreen);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                HandleActivity.GotoSignIn(SplashScreen.this);
                finish();
                Animatoo.INSTANCE.animateSlideLeft(SplashScreen.this);
            }
        }, 3000);
    }
}