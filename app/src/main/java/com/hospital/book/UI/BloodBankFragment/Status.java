package com.hospital.book.UI.BloodBankFragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.DetailsAdapter;
import com.hospital.book.databinding.StatusBinding;

public class Status extends Fragment {

    private StatusBinding binding;
    private DetailsAdapter adapter;
    private ViewModel viewModel;

    public Status() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.status, container, false);
        adapter = new DetailsAdapter();
        viewModel = new ViewModelProvider(getActivity()).get(ViewModel.class);

        InitView();
        return binding.getRoot();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(adapter);
        viewModel.GetExpiredBlood().observe(getActivity(), bloodBankModels -> {
            adapter.setList(bloodBankModels);
            adapter.notifyDataSetChanged();

            if(bloodBankModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }
}