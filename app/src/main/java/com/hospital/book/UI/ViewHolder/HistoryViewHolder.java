package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.hospital.book.databinding.HistoryitemBinding;

public class HistoryViewHolder extends RecyclerView.ViewHolder {

    public HistoryitemBinding binding;

    public HistoryViewHolder(@NonNull HistoryitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
