package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.BloodDonorPagerAdapter;
import com.hospital.book.UI.Adapter.DonorUserAdapter;
import com.hospital.book.UI.BloodDonor.RegisterDonor;
import com.hospital.book.UI.BloodDonor.RegisterFriend;
import com.hospital.book.databinding.BlooddonorsBinding;

public class BloodDonors extends AppCompatActivity {

    private BlooddonorsBinding binding;
    private DonorUserAdapter donorUserAdapter;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.blooddonors);


        donorUserAdapter = new DonorUserAdapter();

        InitView();
        SetFragment();
    }

    private void SetFragment(){
        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var fadapter = new BloodDonorPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        fadapter.AddFragement(new RegisterDonor(), getResources().getString(R.string.RegisterDonor));
        fadapter.AddFragement(new RegisterFriend(), getResources().getString(R.string.RegisterFriend));
        binding.ViewPager.setAdapter(fadapter);
    }

    private void InitView(){
        binding.Toolbar.Title.setText(getResources().getString(R.string.BloodDonor));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(BloodDonors.this);
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(BloodDonors.this);
    }
}