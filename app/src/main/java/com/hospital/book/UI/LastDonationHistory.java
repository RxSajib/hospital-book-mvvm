package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.HistoryAdapter;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.databinding.LastdonationhistoryBinding;

public class LastDonationHistory extends AppCompatActivity {

    private LastdonationhistoryBinding binding;
    private ViewModel viewModel;
    private String UID;
    private HistoryAdapter historyAdapter = new HistoryAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.lastdonationhistory);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        UID = getIntent().getStringExtra(DataManager.UID);

        InitView();
        GetData();
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(LastDonationHistory.this);
        });
        binding.Toolbar.Title.setText("History Of Donation");

        historyAdapter.OnClickState(registerDonationModel -> {
            HandleActivity.BloodDonationHistoryDetails(LastDonationHistory.this, registerDonationModel);
            Animatoo.INSTANCE.animateSlideLeft(LastDonationHistory.this);
        });

        binding.AddBtn.setOnClickListener(view -> {
            HandleActivity.AddRegisterDonation(LastDonationHistory.this);
            Animatoo.INSTANCE.animateSlideLeft(LastDonationHistory.this);
        });
    }

    private void GetData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(historyAdapter);
        viewModel.RegisterDonationGET(UID).observe(this, registerDonationModels -> {
            historyAdapter.setList(registerDonationModels);
            historyAdapter.notifyDataSetChanged();
            if(registerDonationModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(LastDonationHistory.this);
    }
}