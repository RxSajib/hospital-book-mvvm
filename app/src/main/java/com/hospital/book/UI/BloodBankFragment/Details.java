package com.hospital.book.UI.BloodBankFragment;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.DetailsAdapter;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.Widget.BloodBankSelectionDialog;
import com.hospital.book.databinding.DetailsBinding;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;

public class Details extends Fragment{

    private DetailsBinding binding;
    private DetailsAdapter detailsAdapter;
    private ViewModel viewModel;
    private BloodBankSelectionDialog bloodBankSelectionDialog;
    private int Year, Month, Day;
    private int SelectBloodGroup = 0;
    private int SelectBloodCategory = 0;

    public Details() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.details, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        bloodBankSelectionDialog = new BloodBankSelectionDialog();

        GetData();
        InitView();
        return binding.getRoot();
    }

    private void InitView(){
        var calender = Calendar.getInstance(Locale.ENGLISH);
        binding.BloodGroup.setOnClickListener(view -> {
            bloodBankSelectionDialog.show(getActivity().getSupportFragmentManager(), "show");
        });
        bloodBankSelectionDialog.OnclickLisiner(Item -> {

            if(Item == 0){
               OpenBloodDialog();
            }
            if(Item == 1){
                OpenBloodCategoryDialog();
            }
            if(Item == 2){
                Year = calender.get(Calendar.YEAR);
                Month = calender.get(Calendar.MONTH);
                Day = calender.get(Calendar.DAY_OF_MONTH);

                var dialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                    var format = new DecimalFormat("00");
                    var day = format.format(Double.valueOf(i2));
                    var month = format.format(Double.valueOf(i1+1));
                    var year = i;
                    var date = year+"-"+month+"-"+day;

                    SearchByExpDate(date);
                    binding.BloodGroup.setText(date);

                }, Year, Month, Day);
                dialog.show();
            }
            if(Item == 3){
                binding.BloodGroup.setText("");
                GetData();
            }
        });
    }

    private void SearchByExpDate(String Date){
        viewModel.GetBloodBankExpDateGET(Date).observe(getActivity(), bloodBankModels -> {
            detailsAdapter.setList(bloodBankModels);
            detailsAdapter.notifyDataSetChanged();

            if(bloodBankModels != null){
                GONE();
            }else {
               VISIBLE();
            }
        });
    }

    private void GetData(){
        detailsAdapter = new DetailsAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(detailsAdapter);
        viewModel.GetBloodBank().observe(getActivity(), bloodBankModels -> {
            detailsAdapter.setList(bloodBankModels);
            detailsAdapter.notifyDataSetChanged();

            if(bloodBankModels != null){
               GONE();
            }else {
               VISIBLE();
            }
        });

        detailsAdapter.OnClickState(bloodBankModel -> {
            HandleActivity.GotoEditBloodBank(getActivity(), bloodBankModel);
        });
    }

    private void GONE(){
        binding.Message.setVisibility(View.GONE);
        binding.Icon.setVisibility(View.GONE);
    }
    private void VISIBLE(){
        binding.Message.setVisibility(View.VISIBLE);
        binding.Icon.setVisibility(View.VISIBLE);
    }


    private void OpenBloodDialog(){
        var item= getActivity().getResources().getStringArray(R.array.BloodGroup);
        var dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Select blood group");
        dialog.setSingleChoiceItems(item, SelectBloodGroup, (dialogInterface, i) -> {
            SelectBloodGroup = i;
        }).setPositiveButton("Ok", (dialogInterface, i) -> {
            SearchByBloodGroup(item[SelectBloodGroup]);
        }).setNegativeButton("Cancel", (dialogInterface, i) -> {

        });

        dialog.show();
    }

    private void OpenBloodCategoryDialog(){
        var item = getActivity().getResources().getStringArray(R.array.BloodCategory);
        var dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Select blood category");
        dialog.setSingleChoiceItems(item, SelectBloodCategory, (dialogInterface, i) -> {
            SelectBloodCategory = i;
        }).setPositiveButton("Ok", (dialogInterface, i) -> {
            SearchByBloodCategory(item[SelectBloodCategory]);
        }).setNegativeButton("Cancel", (dialogInterface, i) -> {

        });
        dialog.show();
    }

    private void SearchByBloodGroup(String BloodGroup){
        binding.BloodGroup.setText(BloodGroup);
        viewModel.SearchByBloodBankGroupGET(BloodGroup).observe(getActivity(), bloodBankModels -> {
            detailsAdapter.setList(bloodBankModels);
            detailsAdapter.notifyDataSetChanged();

            if(bloodBankModels != null){
                GONE();
            }else {
                VISIBLE();
            }
        });
    }

    private void SearchByBloodCategory(String Category){
        binding.BloodGroup.setText(Category);
        viewModel.SearchByBloodBankCategoryGET(Category).observe(getActivity(), bloodBankModels -> {
            detailsAdapter.setList(bloodBankModels);
            detailsAdapter.notifyDataSetChanged();

            if(bloodBankModels != null){
                GONE();
            }else {
                VISIBLE();
            }
        });
    }

}