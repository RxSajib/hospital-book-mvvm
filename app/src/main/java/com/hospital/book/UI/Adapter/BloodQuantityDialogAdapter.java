package com.hospital.book.UI.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.BloodQuantityModel;
import com.hospital.book.R;
import com.hospital.book.UI.ViewHolder.BloodQuantityViewHolder;
import com.hospital.book.databinding.BloodquantityitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class BloodQuantityDialogAdapter extends RecyclerView.Adapter<BloodQuantityViewHolder>{

    @Setter
    @Getter
    private List<BloodQuantityModel> list;
    private int row_index = -1;
    private OnCLick OnCLick;

    @Inject
    public BloodQuantityDialogAdapter(){

    }

    @NonNull
    @Override
    public BloodQuantityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = BloodquantityitemBinding.inflate(l, parent, false);
        return new BloodQuantityViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BloodQuantityViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.binding.setBloodQuantity(list.get(position));
        holder.itemView.setOnClickListener(view -> {
            OnCLick.Click(list.get(position).getQuantity());
            row_index = position;
            notifyDataSetChanged();
        });

        if (row_index == position) {
            holder.binding.Quantity.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.carbon_red_400));
        } else {
            holder.binding.Quantity.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.carbon_black_38));
        }
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnCLick{
        void Click(String BloodQuantity);
    }
    public void OnClickEvent(OnCLick OnCLick){
        this.OnCLick = OnCLick;
    }
}
