package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.LocationViewModel;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.DistrictNameDataAdapter;
import com.hospital.book.UI.Adapter.LocationDataAdapter;
import com.hospital.book.UI.Adapter.ProvinceNameDataAdapter;
import com.hospital.book.Utils.BetweenTwoDate;
import com.hospital.book.Utils.Toast;
import com.hospital.book.Widget.DatePickerDialogFragment;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.Widget.TermsAndConditionDialog;
import com.hospital.book.databinding.AddregisterasfriendBinding;
import com.hospital.book.databinding.LocationdialogBinding;
import com.nex3z.togglebuttongroup.button.CircularToggle;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

public class AddRegisterAsFriend extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private AddregisterasfriendBinding binding;
    private String[] RegisterDonationDate = {"3 Months", "4 Months", "5 Months", "6 Months", "7 Months", "8 Months", "9 Months", "10 Months", "11 Months", "12 Months"};
    private String SelectRadioItem = null;
    TermsAndConditionDialog termsAndConditionDialog;
    ProgressDialog progressDialog;
    private String Month;
    private ViewModel viewModel;
    private int LastDonationTotalDay;

    private LocationDataAdapter locationDataAdapter;
    private ProvinceNameDataAdapter provinceNameDataAdapter;
    private LocationViewModel locationViewModel;
    private DistrictNameDataAdapter districtNameDataAdapter;
    private String BloodGroup = null;
    private CircularToggle circularToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addregisterasfriend);


        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        termsAndConditionDialog = new TermsAndConditionDialog();
        progressDialog = new ProgressDialog();
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        locationDataAdapter = new LocationDataAdapter();
        provinceNameDataAdapter = new ProvinceNameDataAdapter();
        districtNameDataAdapter = new DistrictNameDataAdapter();



        InitView();
        UploadRegisterDonor();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();

    }

    private void DisableDate() {
        binding.LastDonationDateInput.setVisibility(View.GONE);
    }

    private void EnableDate() {
        binding.LastDonationDateInput.setVisibility(View.VISIBLE);
    }

    private void InitView() {

        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });


        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                binding.District.setText(null);
                binding.Province.setText(null);
                alertdialog.dismiss();
            });
        });


        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(this, getResources().getString(R.string.PleaseSelectYourCountryNameFirst));
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText("");
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });


        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.Message(this, getResources().getString(R.string.SelectCountryName));
            } else if (ProvincesName == "") {
                Toast.Message(this, getResources().getString(R.string.SelectProvinceName));
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });


        binding.MyToolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(this);
        });
        binding.MyToolbar.Title.setText(getResources().getString(R.string.RegisterFriend));
        binding.Never.setChecked(true);
        DisableDate();
        binding.Never.setOnClickListener(view -> {
            DisableDate();
        });
        binding.SelectDate.setOnClickListener(view -> {
            EnableDate();
        });

        var AdapterRegisterDonate = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlayout, R.id.SpinnerTitle, RegisterDonationDate);
        binding.HowManyMonthSpinner.setAdapter(AdapterRegisterDonate);
        binding.HowManyMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                var Name = adapterView.getItemAtPosition(i).toString();
                Month = Name.replaceAll("[^0-9]", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.LastDonationDateInput.setKeyListener(null);

        binding.LastDonationDateInput.setOnClickListener(view -> {
            var dialog = new DatePickerDialogFragment();
            dialog.show(getSupportFragmentManager(), "sdsaasd");
        });
    }


    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
    //todo get all location name

    private void UploadRegisterDonor() {
        binding.CheckBox.setOnClickListener(view1 -> {
            if (binding.CheckBox.isChecked()) {
                termsAndConditionDialog.Show(this);
            }
        });
        termsAndConditionDialog.OnClickLisiner(() -> {

        });
        binding.UploadBtn.setOnClickListener(view -> {

            var sdf = new SimpleDateFormat(DataManager.DatePattern);

            long timestamp = System.currentTimeMillis();
            var calender = Calendar.getInstance(Locale.ENGLISH);
            calender.setTimeInMillis(timestamp);
            var currentdate = DateFormat.format(DataManager.DatePattern, calender).toString();


            RadioButton DateRadioButton;
            var RadioButtonID = binding.RadioGroup.getCheckedRadioButtonId();
            if (RadioButtonID > 0) {
                DateRadioButton = findViewById(RadioButtonID);
                SelectRadioItem = DateRadioButton.getText().toString();
            }


            var GivenName = binding.GivenNamesInput.getText().toString().trim();
            var SureName = binding.SurnameText.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Weight = binding.WeightInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreaName = binding.AreaNameInput.getText().toString().trim();


            var MainPhonenumber = binding.MainNumberInput.getText().toString().trim();
            var RoshanPhoneNumber = binding.RoshanNumberInput.getText().toString().trim();
            var EtisalatPhoneNumber = binding.EtisalatNumberInput.getText().toString().trim();
            var AWCCPhoneNumber = binding.AWCCNumberInput.getText().toString().trim();
            var MTNPhoneNumber = binding.MTNNumberInput.getText().toString().trim();
            var WhatsappNumber = binding.WhatsappNumberInput.getText().toString().trim();
            var EmailAddress = binding.EmailAddressInput.getText().toString().trim();

            if (GivenName.isEmpty()) {
                Toast.Message(this, "Given name is empty");
            } else if (SureName.isEmpty()) {
                Toast.Message(this, "Sure name is empty");
            } else if (BloodGroup == null) {
                Toast.Message(this, "Blood group is empty");
            } else if (Age.isEmpty()) {
                Toast.Message(this, "Age is empty");
            } else if (Weight.isEmpty()) {
                Toast.Message(this, "Weight is empty");
            } else if (Country == "") {
                Toast.Message(this, "Country is empty");
            } else if (Province == "") {
                Toast.Message(this, "Province is empty");
            } else if (District == "") {
                Toast.Message(this, "District is empty");
            } else if (AreaName.isEmpty()) {
                Toast.Message(this, "Area name is empty");
            } else if (Integer.valueOf(Age) < 17) {
                Toast.Message(this, "Age must be at least 18 years");
            } else if (Integer.valueOf(Weight) < 54) {
                Toast.Message(this, "Weight must be at least 55");
            } else {
                //todo ?
                if (binding.CheckBox.isChecked()) {
                    if (SelectRadioItem.equals(getResources().getString(R.string.never))) {
                        UploadData(GivenName, SureName, null, BloodGroup, Age, Weight, Country, Province, District, AreaName,
                                MainPhonenumber, RoshanPhoneNumber, EtisalatPhoneNumber, AWCCPhoneNumber, MTNPhoneNumber, WhatsappNumber,
                                EmailAddress, Month, null, false, null);
                    }
                    if (SelectRadioItem.equals(getResources().getString(R.string.select_a_date))) {
                        var LastDonationDate = binding.LastDonationDateInput.getText().toString().trim();
                        if (LastDonationDate.isEmpty()) {
                            Toast.Message(getApplicationContext(), "Select your last donation date");
                        } else {
                            try {
                                var fromdate = sdf.parse(currentdate);
                                var lastdate = sdf.parse(LastDonationDate);
                                if (fromdate.compareTo(lastdate) < 0) {
                                    Toast.Message(getApplicationContext(), "error your blood donation date is wrong");
                                } else if (fromdate.compareTo(lastdate) == 0) {
                                    Toast.Message(getApplicationContext(), "error your blood donation date and today date was same");
                                } else {
                                    LastDonationTotalDay = BetweenTwoDate.daysBetweenDates(currentdate, LastDonationDate);
                                    UploadData(GivenName, SureName, null, BloodGroup, Age, Weight, Country, Province, District, AreaName,
                                            MainPhonenumber, RoshanPhoneNumber, EtisalatPhoneNumber, AWCCPhoneNumber, MTNPhoneNumber, WhatsappNumber,
                                            EmailAddress, Month, LastDonationDate, false, String.valueOf(LastDonationTotalDay));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                } else {
                    Toast.Message(getApplicationContext(), "you must agree our terms and condition first");
                }

            }


        });
    }

    private void UploadData(String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                            String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                            String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                            String EmailAddress, String DonateMonth, String LastDonationDate, boolean EnableMyNumber, String LastDonationDayCount) {
        progressDialog.ProgressDialog(this);
        viewModel.RegisterDonorNumberExists(MainPhoneNumber).observe(this, aBoolean -> {
            if (!aBoolean) {
                viewModel.RegisterBloodDonorFriend(GivenName, SureName, FatherName, BloodGroup, Age, Width, Country, Province, District, AreaName, MainPhoneNumber,
                                RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, DonateMonth, LastDonationDate, EnableMyNumber, LastDonationDayCount)
                        .observe(this, aBoolean1 -> {
                            if (aBoolean1) {
                                progressDialog.CancelProgressDialog();
                                finish();
                                Animatoo.INSTANCE.animateSlideRight(this);
                            } else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
            } else {
                progressDialog.CancelProgressDialog();
                Toast.Message(this, "Number is already exists");
            }
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        var format = new DecimalFormat("00");
        var day = format.format(Double.valueOf(i2));
        var month = format.format(Double.valueOf(i1));
        binding.LastDonationDateInput.setText(i + "-" + month + "-" + day);
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(AddRegisterAsFriend.this);
    }
}