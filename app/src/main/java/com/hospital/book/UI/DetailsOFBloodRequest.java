package com.hospital.book.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.firebase.auth.FirebaseAuth;
import com.hospital.book.BloodQuantityModel;
import com.hospital.book.BloodRequestModel;
import com.hospital.book.DI.DaggerComponent;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.BloodQuantityDialogAdapter;
import com.hospital.book.Utils.Permission;
import com.hospital.book.Utils.Toast;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.databinding.BloodquantitypickerdialogBinding;
import com.hospital.book.databinding.DetailsofbloodrequestBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DetailsOFBloodRequest extends AppCompatActivity {

    private DetailsofbloodrequestBinding binding;
    private BloodRequestModel model;
    private static int CallPermission = 100;
    @Inject
    ProgressDialog progressDialog;
    private ViewModel viewModel;

    @Inject
    BloodQuantityDialogAdapter bloodQuantityDialogAdapter;
    private static final String TAG = "DetailsOFBloodRequest";
    private List<BloodQuantityModel> quantityModelList = new ArrayList<>();
    private FirebaseAuth Mauth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.detailsofbloodrequest);
        model = (BloodRequestModel) getIntent().getSerializableExtra(DataManager.Data);
        Mauth = FirebaseAuth.getInstance();
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        var component = DaggerComponent.create();
        component.InjectDetailsOFBloodRequest(this);


        for (int i = 100; i <= Integer.valueOf(model.getNumberOFQuantityPints()); i = i+4) {
            var item = new BloodQuantityModel();
            item.setQuantity(String.valueOf(i));
            quantityModelList.add(item);
        }
        bloodQuantityDialogAdapter.setList(quantityModelList);
        bloodQuantityDialogAdapter.OnClickEvent(BloodQuantity -> {

        });

        InitView();
        SetData();
        AcceptBtn();
    }


    private void AcceptBtn(){
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser.getUid().equals(model.getSenderUID())){
            binding.AcceptButton.setEnabled(false);
            binding.AcceptButton.setBackgroundResource(R.color.carbon_green_200);
        }else {
            binding.AcceptButton.setEnabled(true);
            binding.AcceptButton.setBackgroundResource(R.color.carbon_green_500);
        }
    }

    private void SetData() {
        binding.setBloodData(model);
    }

    private void InitView() {

        binding.ShapeBtn.setOnClickListener(view -> {
            if(Permission.PermissionCall(DetailsOFBloodRequest.this, CallPermission)){
                var callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+model.getAttendantContactNumber()));
                startActivity(callIntent);
            }
        });
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(DetailsOFBloodRequest.this);
        });
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(DetailsOFBloodRequest.this);
        });

        binding.Toolbar.Title.setText(getResources().getString(R.string.DetailsOFBloodRequest));
        binding.CancelButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(DetailsOFBloodRequest.this);
        });
        binding.AcceptButton.setOnClickListener(view -> {
            var BloodRequestID = System.currentTimeMillis();
            var dialog = new AlertDialog.Builder(DetailsOFBloodRequest.this);
            var binding = BloodquantitypickerdialogBinding.inflate(getLayoutInflater(), null, false);
            dialog.setView(binding.getRoot());

            var a = dialog.create();
            a.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            a.show();

            binding.SubmitBtn.setOnClickListener(view1 -> {
                var QuantityPint = binding.InputBloodQuantityCC.getText().toString().trim();
                if(QuantityPint.isEmpty()){
                    Toast.Message(DetailsOFBloodRequest.this, getResources().getString(R.string.BloodQuantityPintCC));
                }else {
                    if(Integer.valueOf(QuantityPint) >=  Integer.valueOf(model.getNumberOFQuantityPints())){
                        Toast.Message(DetailsOFBloodRequest.this, "Max blood quantity is "+model.getNumberOFQuantityPints());
                    }else {
                        a.dismiss();
                        progressDialog.ProgressDialog(DetailsOFBloodRequest.this);
                        var count = Integer.valueOf(model.getNumberOFQuantityPints()) - Integer.valueOf(QuantityPint);
                        viewModel.UpdateBloodRequestQuantity(String.valueOf(count), String.valueOf(model.getDocumentKey())).observe(this, aBoolean -> {
                            if(aBoolean){
                                viewModel.MyBloodRequest(model, String.valueOf(QuantityPint), String.valueOf(model.getDocumentKey()), String.valueOf(BloodRequestID)).observe(this, aBoolean1 -> {
                                    if(aBoolean1){

                                        viewModel.SendBloodRequestNotification(model.getDocumentKey(), String.valueOf(QuantityPint), DataManager.BloodRequestAccept, DataManager.BloodRequestAccept, model.getSenderUID(), FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                .observe(DetailsOFBloodRequest.this, aBoolean2 -> {
                                                    if(aBoolean2){
                                                        progressDialog.CancelProgressDialog();
                                                        finish();
                                                        Animatoo.INSTANCE.animateSlideRight(DetailsOFBloodRequest.this);
                                                    }else {
                                                        progressDialog.CancelProgressDialog();
                                                        finish();
                                                        Animatoo.INSTANCE.animateSlideRight(DetailsOFBloodRequest.this);
                                                    }
                                                });

                                    }else {
                                        progressDialog.CancelProgressDialog();
                                    }
                                });

                            }else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
                    }
                }
            });

        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(DetailsOFBloodRequest.this);
    }
}