package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.databinding.DonoritemBinding;

public class DonorItemViewHolder extends RecyclerView.ViewHolder {

    public DonoritemBinding binding;

    public DonorItemViewHolder(@NonNull DonoritemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
