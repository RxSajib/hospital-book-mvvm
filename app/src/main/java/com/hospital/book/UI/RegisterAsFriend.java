package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.hospital.book.R;
import com.hospital.book.databinding.RegisterasfriendBinding;

public class RegisterAsFriend extends AppCompatActivity {

    private RegisterasfriendBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.registerasfriend);

    }
}