package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.databinding.BloodrequestitemBinding;

public class BloodRequestViewHolder extends RecyclerView.ViewHolder {

    public BloodrequestitemBinding binding;

    public BloodRequestViewHolder(@NonNull BloodrequestitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
