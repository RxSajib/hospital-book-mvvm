package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.DI.DaggerComponent;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.Utils.Toast;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.databinding.ForgotpasswordBinding;

import javax.inject.Inject;

public class ForgotPassword extends AppCompatActivity {

    private ForgotpasswordBinding binding;
    @Inject
    ProgressDialog progressDialog;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forgotpassword);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = DaggerComponent.create();
        component.InjectForgotPassword(this);

        InitView();
        ResetPassword();
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSwipeRight(ForgotPassword.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.ForgotPassword));
    }

    private void ResetPassword(){
        binding.ContinueBtn.setOnClickListener(view -> {
            var EmailAddress = binding.EmailInput.getText().toString().trim();
            if(EmailAddress.isEmpty()){
                Toast.Message(ForgotPassword.this, "Email address empty");
            }else {
                progressDialog.ProgressDialog(ForgotPassword.this);
                viewModel.ResetPassword(EmailAddress).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        finish();
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(ForgotPassword.this);
    }
}