package com.hospital.book.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.BloodBankModel;
import com.hospital.book.UI.ViewHolder.StorageViewHolder;
import com.hospital.book.databinding.StorageitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class StorageAdapter extends RecyclerView.Adapter<StorageViewHolder> {
    @Setter @Getter
    private List<BloodBankModel> list;

    @Inject
    public StorageAdapter(){}

    @NonNull
    @Override
    public StorageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = StorageitemBinding.inflate(l, parent, false);
        return new StorageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StorageViewHolder holder, int position) {
        holder.binding.setBloodBank(list.get(position));
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }
}
