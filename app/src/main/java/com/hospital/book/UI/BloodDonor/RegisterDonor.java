package com.hospital.book.UI.BloodDonor;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.DonorItemAdapter;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.Utils.Toast;
import com.hospital.book.databinding.BloodregisterdonorBinding;
import com.hospital.book.databinding.RegisterdonorBinding;

public class RegisterDonor extends Fragment {

    private BloodregisterdonorBinding binding;
    private ViewModel viewModel;
    private DonorItemAdapter donorItemAdapter;

    public RegisterDonor() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bloodregisterdonor, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        donorItemAdapter = new DonorItemAdapter();

        InitView();
        Getdata();

        return binding.getRoot();
    }

    private void  Getdata(){
        viewModel.RegisterAsDonorGET().observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();

            if(donorModels != null){
                InVisibleInfo();
            }else {
                VisibleInfo(R.drawable.ic_user, "No user found");
            }
        });
    }
    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(donorItemAdapter);


        donorItemAdapter.OnClickState(model -> {
            HandleActivity.GotoDetailsOFRegisterBloodDonor(getActivity(), model);
            Animatoo.INSTANCE.animateSlideLeft(getActivity());
        });


        binding.SearchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if(binding.SearchInput.getText().toString().trim().isEmpty()){
                    Toast.Message(getActivity(), "Number is empty");
                }else {
                    SearchByNumber(binding.SearchInput.getText().toString().trim());
                }
                return true;
            }
            return false;
        });

        binding.SearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                var data = editable.toString();
                if(data.isEmpty()){
                    Getdata();
                }
            }
        });
    }

    private void SearchByNumber(String Number){
        viewModel.SearchDonorUserByNumber(DataManager.RegisterAsDonor, Number).observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();

            if(donorModels != null){
                InVisibleInfo();
            }else {
                VisibleInfo(R.drawable.ic_search, "Not found anythings");
            }
        });
    }


    private void InVisibleInfo(){
        binding.Icon.setVisibility(View.GONE);
        binding.Message.setVisibility(View.GONE);
    }

    private void VisibleInfo(int ImageResID, String Message){
        binding.Icon.setImageResource(ImageResID);
        binding.Message.setText(Message);

        binding.Message.setVisibility(View.VISIBLE);
        binding.Icon.setVisibility(View.VISIBLE);
    }
}