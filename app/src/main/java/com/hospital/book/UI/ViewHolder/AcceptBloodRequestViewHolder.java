package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.hospital.book.databinding.AcceptbloodrequestitemBinding;

public class AcceptBloodRequestViewHolder extends RecyclerView.ViewHolder{

    public AcceptbloodrequestitemBinding binding;

    public AcceptBloodRequestViewHolder(@NonNull AcceptbloodrequestitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
