package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.databinding.StorageitemBinding;

public class StorageViewHolder extends RecyclerView.ViewHolder {

    public StorageitemBinding binding;

    public StorageViewHolder(@NonNull StorageitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
