package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.hospital.book.databinding.LocationitemBinding;

public class LocationViewHolder extends RecyclerView.ViewHolder {

    public LocationitemBinding binding;

    public LocationViewHolder(@NonNull LocationitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
