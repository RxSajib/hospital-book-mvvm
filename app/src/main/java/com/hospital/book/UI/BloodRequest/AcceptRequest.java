package com.hospital.book.UI.BloodRequest;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.AcceptBloodRequestAdapter;
import com.hospital.book.Utils.Toast;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.databinding.AcceptrequestBinding;

public class AcceptRequest extends Fragment {

    private AcceptrequestBinding binding;
    private ViewModel viewModel;
    private AcceptBloodRequestAdapter acceptBloodRequestAdapter;
    private ProgressDialog progressDialog;


    public AcceptRequest() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.acceptrequest, container, false);
        acceptBloodRequestAdapter = new AcceptBloodRequestAdapter();
        progressDialog = new ProgressDialog();
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        InitView();
        return binding.getRoot();

    }
    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(acceptBloodRequestAdapter);

        viewModel.GetMyAcceptBloodRequest().observe(getActivity(), bloodRequestModels -> {
            acceptBloodRequestAdapter.setList(bloodRequestModels);
            acceptBloodRequestAdapter.notifyDataSetChanged();
            if(bloodRequestModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.message.setVisibility(View.VISIBLE);
            }
        });

        acceptBloodRequestAdapter.OnClickState(bloodRequestModel -> {
            var dialog = new AlertDialog.Builder(getActivity());
            dialog.setTitle("Can’t go to Patient?");
            dialog.setMessage("Send back request");

            dialog.setPositiveButton("Send Back request", (dialogInterface, i) -> {
                progressDialog.ProgressDialog(getActivity());
                viewModel.GetSingleBloodRequest(String.valueOf(bloodRequestModel.getDocumentID()))
                        .observe(getActivity(), bloodRequestModel1 -> {
                            if(bloodRequestModel1 != null){
                                var count = Integer.valueOf(bloodRequestModel1.getNumberOFQuantityPints()) + Integer.valueOf(bloodRequestModel.getNumberOFQuantityPints());
                                viewModel.UpdateBloodRequestQuantity(String.valueOf(count), String.valueOf(bloodRequestModel.getDocumentID()))
                                        .observe(getActivity(), aBoolean -> {
                                            if(aBoolean){
                                                viewModel.MyAcceptedBloodRequestDelete(String.valueOf(bloodRequestModel.getDocumentKey()))
                                                        .observe(getActivity(), aBoolean1 -> {
                                                            if(aBoolean1){
                                                                progressDialog.CancelProgressDialog();
                                                            }else {
                                                                progressDialog.CancelProgressDialog();
                                                            }
                                                        });
                                            }else {
                                                progressDialog.CancelProgressDialog();
                                            }
                                        });
                            }
                        });

            });
            dialog.setNegativeButton("Cancel", (dialogInterface, i) -> {
                Toast.Message(getActivity(), bloodRequestModel.getNumberOFQuantityPints());
            });

            var d = dialog.create();
            d.show();

        });
    }
}