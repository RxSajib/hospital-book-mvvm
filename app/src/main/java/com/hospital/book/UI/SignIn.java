package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.DI.DaggerComponent;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.Utils.Toast;
import com.hospital.book.Widget.ProgressDialog;
import com.hospital.book.databinding.SigninBinding;

import javax.inject.Inject;

public class SignIn extends AppCompatActivity {

    private SigninBinding binding;
    @Inject
    ProgressDialog progressDialog;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.signin);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = DaggerComponent.create();
        component.InjectSignIn(this);

        InitView();
        SignInAccount();
    }

    private void InitView(){
        binding.ForGotPassword.setOnClickListener(view -> {
            HandleActivity.GotoForgotPassword(SignIn.this);
            Animatoo.INSTANCE.animateSlideLeft(SignIn.this);
        });
    }

    private void SignInAccount(){
        binding.SignInBtn.setOnClickListener(view -> {
            var Email = binding.EmailInput.getText().toString().trim();
            var Password = binding.PasswordInput.getText().toString().trim();

            if(Email.isEmpty()){
                Toast.Message(getApplicationContext(), getResources().getString(R.string.EmailEmptyToastMessage));
            }else if(Password.isEmpty()){
                Toast.Message(getApplicationContext(), getResources().getString(R.string.PasswordEmptyToastMessage));
            }else {
                progressDialog.ProgressDialog(SignIn.this);
                viewModel.SignInEmailPassword(Email, Password).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        HandleActivity.GotoHome(SignIn.this);
                        finish();
                        Animatoo.INSTANCE.animateSlideLeft(SignIn.this);
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }
}