package com.hospital.book.UI.BloodRequest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.R;
import com.hospital.book.UI.Adapter.RequestViewPagerAdapter;
import com.hospital.book.databinding.RequestofbloodhomeBinding;

public class RequestOFBloodHome extends AppCompatActivity {

    private RequestofbloodhomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.requestofbloodhome);

        SetFragment();
        InitView();
    }


    private void InitView() {
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(RequestOFBloodHome.this);
        });
        binding.Toolbar.Title.setText(getResources().getString(R.string.BloodRequest));
    }

    private void SetFragment() {
        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var fadapter = new RequestViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        fadapter.AddFragement(new ReceiveRequest(), getResources().getString(R.string.ReceiveRequest));
        fadapter.AddFragement(new AcceptRequest(), getResources().getString(R.string.MyRequest));
        binding.ViewPager.setAdapter(fadapter);
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(RequestOFBloodHome.this);
    }
}