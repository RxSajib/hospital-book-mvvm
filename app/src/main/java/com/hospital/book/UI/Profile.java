package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.R;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.Utils.Toast;
import com.hospital.book.databinding.ProfileBinding;

public class Profile extends AppCompatActivity {

    private ProfileBinding binding;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.profile);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        GetProfileInfo();
        InitView();
        UpdateProfile();
    }

    private void GetProfileInfo(){
        viewModel.GetCurrentUser().observe(this, user -> {
            if(user != null){
                binding.setUser(user);
            }
        });
    }

    private void UpdateProfile(){
        binding.UpdateBtn.setOnClickListener(view -> {
            var Name = binding.NameInput.getText().toString().trim();
            var Location = binding.LocationInput.getText().toString().trim();

            if(Name.isEmpty()){
                Toast.Message(Profile.this, "Name empty");
            }else if(Location.isEmpty()){
                Toast.Message(Profile.this, "Location empty");
            }else {

            }
        });
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.INSTANCE.animateSlideRight(Profile.this);
        });
        binding.Toolbar.Title.setText("Profile");

        binding.LogoutBtn.setOnClickListener(view -> {
            var dialog = new AlertDialog.Builder(Profile.this);
            dialog.setTitle("Are you sure?");
            dialog.setMessage("you want to logout your account");

            dialog.setPositiveButton("Continue", (dialogInterface, i) -> {
                viewModel.LogOutAccount().observe(this, aBoolean -> {
                    HandleActivity.GotoSplashScreen(Profile.this);
                    finish();
                    Animatoo.INSTANCE.animateSlideRight(Profile.this);
                });
            });
            dialog.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());

            var d = dialog.create();
            d.show();

        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.INSTANCE.animateSlideRight(Profile.this);
    }
}