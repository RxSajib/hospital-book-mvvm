package com.hospital.book.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.hospital.book.R;
import com.hospital.book.databinding.BloodsearchBinding;

public class BloodSearch extends AppCompatActivity {

    private BloodsearchBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.bloodsearch);

        InitView();
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {

        });
    }
}