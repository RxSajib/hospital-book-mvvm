package com.hospital.book.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.BloodBankModel;
import com.hospital.book.UI.ViewHolder.DetailsViewHolder;
import com.hospital.book.databinding.DetailsitemBinding;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsViewHolder> {
    @Setter
    @Getter
    private List<BloodBankModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public DetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = DetailsitemBinding.inflate(l, parent, false);
        return new DetailsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailsViewHolder holder, int position) {
        holder.binding.setBloodBank(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Clikc(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public interface OnClick{
        void Clikc(BloodBankModel bloodBankModel);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
