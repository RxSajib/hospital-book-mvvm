package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.hospital.book.databinding.DetailsitemBinding;

public class DetailsViewHolder extends RecyclerView.ViewHolder {

    public DetailsitemBinding binding;

    public DetailsViewHolder(@NonNull DetailsitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
