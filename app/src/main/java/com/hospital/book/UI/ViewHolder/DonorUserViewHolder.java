package com.hospital.book.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hospital.book.databinding.DonoruseritemBinding;

public class DonorUserViewHolder extends RecyclerView.ViewHolder {
    public DonoruseritemBinding binding;

    public DonorUserViewHolder(@NonNull DonoruseritemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
