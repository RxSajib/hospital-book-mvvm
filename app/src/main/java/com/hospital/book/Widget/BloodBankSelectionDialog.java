package com.hospital.book.Widget;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.hospital.book.R;

import javax.inject.Inject;

public class BloodBankSelectionDialog extends DialogFragment {

    @Inject
    public BloodBankSelectionDialog(){

    }

    private Onclick Onclick;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        var alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setTitle("Select Filter Option");

        var item= getActivity().getResources().getStringArray(R.array.FilterBloodBank);
        alertdialog.setItems(item, (dialogInterface, i) -> {
            Onclick.Click(i);
        });
        return alertdialog.create();

    }

    public interface Onclick{
        void Click(int Item);
    }

    public void OnclickLisiner(Onclick Onclick){
        this.Onclick = Onclick;
    }
}
