package com.hospital.book.Widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import com.hospital.book.R;

public class BloodDialog extends DialogFragment {

    int position = 0;

    public interface SingleChoiseLisiner{
        void OnPositionButtonClick(String[] list, int position);
        void OnNegativeButtonClick();
    }
    SingleChoiseLisiner lisiner;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            lisiner = (SingleChoiseLisiner) context;
        }catch (Exception e){
           new ClassCastException(context.toString());
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        var alertdialog = new AlertDialog.Builder(getActivity());
        var list = getActivity().getResources().getStringArray(R.array.BloodGroup);

        alertdialog.setTitle("Select blood group")
                .setSingleChoiceItems(list, position, (dialogInterface, i) -> {
                    position = i;
                })
                .setPositiveButton("Ok", (dialogInterface, i) -> {
                    lisiner.OnPositionButtonClick(list, position);
                }).setNegativeButton("Cancel", (dialogInterface, i) -> {
                    lisiner.OnNegativeButtonClick();
                });
        return alertdialog.create();
    }


}
