package com.hospital.book.Widget;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.hospital.book.Data.DataManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatePickerDialogFragment extends DialogFragment {


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        var cal = Calendar.getInstance();

        cal.add(Calendar.DATE, 1);

        Date date = cal.getTime();

        var format1 = new SimpleDateFormat(DataManager.DatePattern);

        var date1 = format1.format(date);

        var calender = Calendar.getInstance(Locale.ENGLISH);

        var year = calender.get(Calendar.YEAR);
        var month = calender.get(Calendar.MONTH);
        var day = calender.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(),  (DatePickerDialog.OnDateSetListener) getParentFragment(),
                year, month, day);

    }
}
