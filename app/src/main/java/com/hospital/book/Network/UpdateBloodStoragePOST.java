package com.hospital.book.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Utils.Toast;

import java.util.HashMap;

public class UpdateBloodStoragePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference BloodBankRe;
    private FirebaseAuth Mauth;

    public UpdateBloodStoragePOST(Application application){
        this.application = application;
        BloodBankRe = FirebaseFirestore.getInstance().collection(DataManager.BloodStorage);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UpdateBloodStorage(long DocumentID, String TotalQuantity){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var map = new HashMap<String, Object>();
            map.put(DataManager.Quantity, TotalQuantity);

            BloodBankRe.document(String.valueOf(DocumentID))
                    .update(map).addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                       data.setValue(false);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }
}
