package com.hospital.book.Network.ViewModel;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.hospital.book.AcceptBloodRequestModel;
import com.hospital.book.BloodBankModel;
import com.hospital.book.BloodRequestModel;
import com.hospital.book.CountryNameModel;
import com.hospital.book.DistrictNameModel;
import com.hospital.book.DonorModel;
import com.hospital.book.Network.AcceptBloodRequestGET;
import com.hospital.book.Network.BloodBankExpDateGET;
import com.hospital.book.Network.BloodBankExpiredGET;
import com.hospital.book.Network.BloodBankPOST;
import com.hospital.book.Network.BloodDonationHistoryUpdate;
import com.hospital.book.Network.BloodRequestGET;
import com.hospital.book.Network.BloodRequestNotification;
import com.hospital.book.Network.BloodRequestQuantityUpdate;
import com.hospital.book.Network.DeleteBloodBank;
import com.hospital.book.Network.DonorUserSearchByNumber;
import com.hospital.book.Network.LogoutAccount;
import com.hospital.book.Network.MyAcceptBloodRequestDelete;
import com.hospital.book.Network.MyBloodRequestPOST;
import com.hospital.book.Network.RegisterAsDonorUpdate;
import com.hospital.book.Network.RegisterAsFriendGET;
import com.hospital.book.Network.RegisterDonationGET;
import com.hospital.book.Network.RegisterDonationImageUpdate;
import com.hospital.book.Network.RegisterDonorAsFriendPOST;
import com.hospital.book.Network.RegisterDonorGET;
import com.hospital.book.Network.RegisterDonorNumberExistsGET;
import com.hospital.book.Network.SearchBloodGET;
import com.hospital.book.Network.SearchByBloodBankCategoryGET;
import com.hospital.book.Network.SearchByBloodBankGroupGET;
import com.hospital.book.Network.SingleBloodRequestGET;
import com.hospital.book.Network.StorageBloodPOST;
import com.hospital.book.Network.BloodbankGET;
import com.hospital.book.Network.CountryNameGET;
import com.hospital.book.Network.DistrictNameGET;
import com.hospital.book.Network.EmailPasswordSignInAccount;
import com.hospital.book.Network.FilterOrganizationGET;
import com.hospital.book.Network.OrganizationGET;
import com.hospital.book.Network.OrganizationTypeDataGET;
import com.hospital.book.Network.ProvinceNameGET;
import com.hospital.book.Network.SearchBloodStorageGET;
import com.hospital.book.Network.SearchTelephonyManager;
import com.hospital.book.Network.UpdateBloodStoragePOST;
import com.hospital.book.Network.UserExistsGET;
import com.hospital.book.Network.CurrentUserGET;
import com.hospital.book.Network.UserGET;
import com.hospital.book.OrganizationModel;
import com.hospital.book.OrganizationTypeModel;
import com.hospital.book.ProfileModel;
import com.hospital.book.ProvinceNameModel;
import com.hospital.book.RegisterDonationModel;
import com.hospital.book.User;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    private LogoutAccount logoutAccount;
    private RegisterDonationGET registerDonationGET;
    private RegisterDonationImageUpdate registerDonationImageUpdate;
    private BloodDonationHistoryUpdate bloodDonationHistoryUpdate;
    private RegisterAsDonorUpdate registerAsDonorUpdate;
    private RegisterAsFriendGET registerAsFriendGET;
    private DonorUserSearchByNumber donorUserSearchByNumber;
    private RegisterDonorGET registerDonorGET;
    private MyAcceptBloodRequestDelete myAcceptBloodRequestDelete;
    private SingleBloodRequestGET singleBloodRequestGET;
    private AcceptBloodRequestGET acceptBloodRequestGET;
    private BloodRequestNotification bloodRequestNotification;
    private MyBloodRequestPOST myBloodRequestPOST;
    private BloodRequestQuantityUpdate bloodRequestQuantityUpdate;

    private RegisterDonorAsFriendPOST registerDonorAsFriendPOST;
    private RegisterDonorNumberExistsGET registerDonorNumberExistsGET;
    private SearchByBloodBankCategoryGET searchByBloodBankCategoryGET;
    private SearchByBloodBankGroupGET searchByBloodBankGroupGET;
    private BloodBankExpDateGET bloodBankExpDateGET;
    private BloodRequestGET bloodRequestGET;
    private UserGET userGET;
    private SearchBloodGET searchBloodGET;
    private CurrentUserGET currentUserGET;
    private DeleteBloodBank deleteBloodBank;
    private BloodBankExpiredGET bloodBankExpiredGET;
    private BloodBankPOST bloodBankPOST;
    private BloodbankGET  bloodbankGET;
    private UpdateBloodStoragePOST updateBloodStoragePOST;
    private SearchBloodStorageGET searchBloodStorageGET;
    private StorageBloodPOST storageBloodPOST;
    private FilterOrganizationGET filterOrganizationGET;
    private OrganizationTypeDataGET organizationTypeDataGET;
    private DistrictNameGET districtNameGET;
    private ProvinceNameGET provinceNameGET;
    private CountryNameGET countryNameGET;
    private OrganizationGET organizationGET;
    private SearchTelephonyManager searchTelephonyManager;
    private ForgotPasswordPOST forgotPasswordPOST;
    private EmailPasswordSignInAccount emailPasswordSignInAccount;
    public UserExistsGET userExistsGET;

    public ViewModel(@NonNull Application application) {
        super(application);

        logoutAccount = new LogoutAccount(application);
        registerDonationGET = new RegisterDonationGET(application);
        registerDonationImageUpdate = new RegisterDonationImageUpdate(application);
        bloodDonationHistoryUpdate = new BloodDonationHistoryUpdate(application);
        registerAsDonorUpdate = new RegisterAsDonorUpdate(application);
        registerAsFriendGET = new RegisterAsFriendGET(application);
        donorUserSearchByNumber = new DonorUserSearchByNumber(application);
        registerDonorGET = new RegisterDonorGET(application);
        myAcceptBloodRequestDelete = new MyAcceptBloodRequestDelete(application);
        singleBloodRequestGET = new SingleBloodRequestGET(application);
        acceptBloodRequestGET = new AcceptBloodRequestGET(application);
        bloodRequestNotification = new BloodRequestNotification(application);
        myBloodRequestPOST = new MyBloodRequestPOST(application);
        bloodRequestQuantityUpdate = new BloodRequestQuantityUpdate(application);
        registerDonorAsFriendPOST = new RegisterDonorAsFriendPOST(application);
        registerDonorNumberExistsGET = new RegisterDonorNumberExistsGET(application);
        searchByBloodBankCategoryGET = new SearchByBloodBankCategoryGET(application);
        searchByBloodBankGroupGET = new SearchByBloodBankGroupGET(application);
        bloodBankExpDateGET = new BloodBankExpDateGET(application);
        bloodRequestGET = new BloodRequestGET(application);
        userGET = new UserGET(application);
        searchBloodGET = new SearchBloodGET(application);
        currentUserGET = new CurrentUserGET(application);
        deleteBloodBank = new DeleteBloodBank(application);
        bloodBankExpiredGET = new BloodBankExpiredGET(application);
        bloodBankPOST = new BloodBankPOST(application);
        bloodbankGET = new BloodbankGET(application);
        updateBloodStoragePOST = new UpdateBloodStoragePOST(application);
        searchBloodStorageGET = new SearchBloodStorageGET(application);
        storageBloodPOST = new StorageBloodPOST(application);
        filterOrganizationGET = new FilterOrganizationGET(application);
        organizationTypeDataGET = new OrganizationTypeDataGET(application);
        districtNameGET = new DistrictNameGET(application);
        provinceNameGET = new ProvinceNameGET(application);
        countryNameGET = new CountryNameGET(application);
        organizationGET = new OrganizationGET(application);
        searchTelephonyManager = new SearchTelephonyManager(application);
        forgotPasswordPOST = new ForgotPasswordPOST(application);
        emailPasswordSignInAccount = new EmailPasswordSignInAccount(application);
        userExistsGET = new UserExistsGET(application);
    }

    public LiveData<Boolean> LogOutAccount(){
        return logoutAccount.LogOutAccount();
    }
    public LiveData<List<RegisterDonationModel>> RegisterDonationGET(String UID){
        return registerDonationGET.GetRegisterDonation(UID);
    }
    public LiveData<Boolean> RegisterDonationImageUpdate(String UID, Uri ImageUri){
        return registerDonationImageUpdate.RegisterDonationImageUpdate(UID, ImageUri);
    }
    public LiveData<Boolean> RegisterDonationUpdate(String UID, String BloodDonationDate, String Country, String District, String HospitalName, String Province, String QuantityOfBlood){
        return bloodDonationHistoryUpdate.RegisterDonationUpdate(UID, BloodDonationDate, Country, District, HospitalName, Province, QuantityOfBlood);
    }
    public LiveData<Boolean> UpdateRegisterAsDonor(String UID,String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Weight, String Country, String Province, String District, String AreaName, String PhoneNumber, String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber, String EmailAddress, String Month){
        return registerAsDonorUpdate.UpdateRegisterAsDonor(UID, GivenName, SureName, FatherName, BloodGroup, Age, Weight, Country, Province, District, AreaName, PhoneNumber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, Month);
    }
    public LiveData<List<DonorModel>> RegisterAsFriendGET(){
        return registerAsFriendGET.RegisterAsFriendGET();
    }
    public LiveData<List<DonorModel>> SearchDonorUserByNumber(String Type, String Number){
        return donorUserSearchByNumber.SearchDonorUserByNumber(Type, Number);
    }
    public LiveData<List<DonorModel>> RegisterAsDonorGET(){
        return registerDonorGET.RegisterDonorGET();
    }
    public LiveData<Boolean> MyAcceptedBloodRequestDelete(String DocumentID){
        return myAcceptBloodRequestDelete.MyAcceptBloodRequestDelete(DocumentID);
    }
    public LiveData<BloodRequestModel> GetSingleBloodRequest(String DocumentID){
        return singleBloodRequestGET.GetSingleBloodRequest(DocumentID);
    }
    public LiveData<List<AcceptBloodRequestModel>> GetMyAcceptBloodRequest(){
        return acceptBloodRequestGET.GetMyBloodAcceptRequest();
    }
    public LiveData<Boolean> SendBloodRequestNotification(long DocumentKey, String Message, String MessageTopic, String MessageType, String ReceiverUID, String SenderUID){
        return bloodRequestNotification.SendBloodRequestNotification(DocumentKey, Message, MessageTopic, MessageType, ReceiverUID, SenderUID);
    }
    public LiveData<Boolean> MyBloodRequest(BloodRequestModel bloodRequestModel, String Quantity, String DocumentID, String BloodRequestID){
        return myBloodRequestPOST.MyBloodRequest(bloodRequestModel, Quantity, DocumentID, BloodRequestID);
    }
    public LiveData<Boolean> UpdateBloodRequestQuantity(String BloodQuantity, String DocumentID){
        return bloodRequestQuantityUpdate.UpdateBloodRequestQuantity(BloodQuantity, DocumentID);
    }
    public LiveData<Boolean> RegisterBloodDonorFriend(String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                                                      String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                                                      String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                                                      String EmailAddress, String DonateMonth, String LastDonationDate, boolean EnableMyNumber, String LastDonationDayCount){
        return registerDonorAsFriendPOST.RegisterBloodDonorFriend(GivenName, SureName, FatherName, BloodGroup, Age, Width, Country, Province, District, AreaName, MainPhoneNumber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, DonateMonth, LastDonationDate, EnableMyNumber, LastDonationDayCount);
    }
    public LiveData<Boolean> RegisterDonorNumberExists(String MainPhoneNumber){
        return registerDonorNumberExistsGET.SearchDonorNumber(MainPhoneNumber);
    }
    public LiveData<List<BloodBankModel>> SearchByBloodBankCategoryGET(String BloodCategory){
        return searchByBloodBankCategoryGET.SearchByBloodBankCategory(BloodCategory);
    }
    public LiveData<List<BloodBankModel>> SearchByBloodBankGroupGET(String BloodGroup){
        return searchByBloodBankGroupGET.SearchByBloodBankGroupGET(BloodGroup);
    }
    public LiveData<List<BloodBankModel>> GetBloodBankExpDateGET(String ExpDate){
        return bloodBankExpDateGET.GetBloodBankByExpDate(ExpDate);
    }
    public LiveData<List<BloodRequestModel>> GetBloodRequest(){
        return bloodRequestGET.GetBloodRequest();
    }
    public LiveData<ProfileModel> GetUserInfo(String UID){
        return userGET.GetUserInfo(UID);
    }
    public LiveData<List<DonorModel>> SearchBloodGroup(String BloodName, String Country, String Province, String District){
        return searchBloodGET.SearchBloodDonor(BloodName, Country, Province, District);
    }
    public LiveData<User> GetCurrentUser(){
        return currentUserGET.GetCurrentUser();
    }
    public LiveData<Boolean> DeleteBloodBank(long DocumentID){
        return deleteBloodBank.DeleteBloodBank(DocumentID);
    }
    public LiveData<List<BloodBankModel>> GetExpiredBlood(){
        return bloodBankExpiredGET.GetExpiredBlood();
    }
    public LiveData<Boolean> UploadBlood(String HospitalLogo, String HospitalName, String Location,long Timestamp, String BloodCategory, String BloodGroup, String TagNumber, String Quantity, String DonorName, String DonorPhoneNumber
            , String BloodDonationDate, String BloodExpiry, String Status){
        return bloodBankPOST.BloodBankPOST(HospitalLogo, HospitalName, Location, Timestamp, BloodCategory, BloodGroup, TagNumber, Quantity, DonorName, DonorPhoneNumber, BloodDonationDate, BloodExpiry, Status);
    }
    public LiveData<List<BloodBankModel>> GetBloodBank(){
        return bloodbankGET.GetBloodBank();
    }
    public LiveData<Boolean> UpdateBloodStoragePost(long DocumentID, String Quantity){
        return updateBloodStoragePOST.UpdateBloodStorage(DocumentID, Quantity);
    }

    public LiveData<List<BloodBankModel>> SearchBloodStorage(String Category, String BloodGroup){
        return searchBloodStorageGET.SearchBloodStorage(Category, BloodGroup);
    }

    public LiveData<Boolean> StorageBloodPOST(String HospitalLogo, String HospitalName, String Location, long Timestamp, String BloodCategory, String BloodGroup, String TagNumber, String Quantity, String DonorName, String DonorPhoneNumber
            , String BloodDonationDate, String BloodExpiry, String Status){
        return storageBloodPOST.BloodStoragePOST(HospitalLogo, HospitalName, Location, Timestamp, BloodCategory, BloodGroup, TagNumber, Quantity, DonorName, DonorPhoneNumber, BloodDonationDate, BloodExpiry, Status);
    }

    public LiveData<List<OrganizationModel>> FilterOrganizationData(String Country, String Province, String District, String OrganizationType){
        return filterOrganizationGET.FilterOrganization(Country, Province, District, OrganizationType);
    }
    public LiveData<List<OrganizationTypeModel>> GetOrganizationType(){
        return organizationTypeDataGET.GetOrganizationType();
    }
    public LiveData<List<DistrictNameModel>> GetDistrictName(String CountryName, String Province){
        return districtNameGET.GetDistrictName(CountryName, Province);
    }

    public LiveData<List<ProvinceNameModel>> GetProvinceName(String CountryName){
        return provinceNameGET.GetProvinceData(CountryName);
    }
    public LiveData<List<CountryNameModel>> GetCountryName(){
        return countryNameGET.GetCountryNameData();
    }
    public LiveData<List<OrganizationModel>> Getorganization(){
        return organizationGET.GetOrganization();
    }
    public LiveData<List<OrganizationModel>> SearchOrganization(String OrgName){
        return searchTelephonyManager.SearchTelephonyManager(OrgName);
    }

    public LiveData<Boolean> ResetPassword(String Email){
        return forgotPasswordPOST.ForgotPassword(Email);
    }
    public LiveData<Boolean> SignInEmailPassword(String Email, String Password){
        return emailPasswordSignInAccount.SignInEmailPassword(Email, Password);
    }
    public LiveData<Boolean> UserExists(){
        return userExistsGET.UserExists();
    }
}
