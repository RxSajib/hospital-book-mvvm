package com.hospital.book.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.AcceptBloodRequestModel;
import com.hospital.book.Data.DataManager;

import java.util.List;

public class AcceptBloodRequestGET {


    private Application application;
    private MutableLiveData<List<AcceptBloodRequestModel>> data;
    private CollectionReference MyBloodRequestRef;
    private FirebaseAuth Mauth;

    public AcceptBloodRequestGET(android.app.Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MyBloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.MyBloodRequest);
    }

    public LiveData<List<AcceptBloodRequestModel>> GetMyBloodAcceptRequest(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = MyBloodRequestRef.whereEqualTo(DataManager.SenderUID, FirebaseUser.getUid());
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(AcceptBloodRequestModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
