package com.hospital.book.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.Data.DataManager;
import com.hospital.book.R;
import com.hospital.book.Utils.Toast;

import java.util.HashMap;

public class BloodRequestQuantityUpdate {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth MAuth;
    private CollectionReference BloodRequestRef;

    public BloodRequestQuantityUpdate(android.app.Application application){
        this.application = application;
        MAuth = FirebaseAuth.getInstance();
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<Boolean> UpdateBloodRequestQuantity(String BloodQuantity, String DocumentID){
        data = new MutableLiveData<>();
        var FirebaseUser = MAuth.getCurrentUser();
        if(FirebaseUser != null){
            var map = new HashMap<String, Object>();
            map.put(DataManager.NumberOFQuantityPints, BloodQuantity);
            BloodRequestRef.document(DocumentID).update(map)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                            Toast.Message(application, application.getResources().getString(R.string.Success));
                        }else {
                            data.setValue(false);
                            Toast.Message(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.Message(application, e.getMessage());
                    });
        }
        return data;
    }
}
