package com.hospital.book.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.Data.DataManager;
import com.hospital.book.User;

public class CurrentUserGET {

    private Application application;
    private MutableLiveData<User> data;
    private CollectionReference HospitalRef;
    private FirebaseAuth Mauth;

    public CurrentUserGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        HospitalRef = FirebaseFirestore.getInstance().collection(DataManager.Hospital);
    }

    public LiveData<User> GetCurrentUser(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            HospitalRef.document(FirebaseUser.getUid())
                    .addSnapshotListener((value, error) -> {
                        if(error != null){
                            data.setValue(null);
                            return;
                        }
                        if(value.exists()){
                            data.setValue(value.toObject(User.class));
                        }else {
                            data.setValue(null);
                        }
                    });
        }
        return data;
    }
}
