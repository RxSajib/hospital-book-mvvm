package com.hospital.book.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.BloodBankModel;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Utils.Toast;

import java.util.List;

public class SearchBloodStorageGET {

    private Application application;
    private MutableLiveData<List<BloodBankModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference BloodBankRef;

    public SearchBloodStorageGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodBankRef = FirebaseFirestore.getInstance().collection(DataManager.BloodStorage);
    }

    public LiveData<List<BloodBankModel>> SearchBloodStorage(String Category, String BloodGroup){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = BloodBankRef.whereEqualTo(DataManager.BloodCategory, Category).whereEqualTo(DataManager.BloodGroup, BloodGroup);
            q.get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    if(task.getResult().isEmpty()){
                        data.setValue(null);
                    }else {
                        data.setValue(task.getResult().toObjects(BloodBankModel.class));
                    }
                }else {
                    data.setValue(null);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(null);
                Toast.Message(application, e.getMessage());
            });
        }
        return data;
    }
}
