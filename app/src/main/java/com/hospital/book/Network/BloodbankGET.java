package com.hospital.book.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.BloodBankModel;
import com.hospital.book.Data.DataManager;

import java.util.List;

public class BloodbankGET {

    private Application application;
    private MutableLiveData<List<BloodBankModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference BloodBankRef;

    public BloodbankGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodBankRef = FirebaseFirestore.getInstance().collection(DataManager.BloodStorage);
    }

    public LiveData<List<BloodBankModel>> GetBloodBank(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            BloodBankRef.addSnapshotListener((value, error) -> {
               if(error != null){
                   data.setValue(null);
                   return;
               }
               if(!value.isEmpty()){
                   for(var ds : value.getDocumentChanges()){
                       if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                           data.setValue(value.toObjects(BloodBankModel.class));
                       }
                   }
               }else {
                   data.setValue(null);
               }
            });
        }
        return data;
    }
}
