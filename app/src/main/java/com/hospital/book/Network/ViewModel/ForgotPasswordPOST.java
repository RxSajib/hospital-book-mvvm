package com.hospital.book.Network.ViewModel;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.hospital.book.Utils.Toast;

public class ForgotPasswordPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public ForgotPasswordPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> ForgotPassword(String EmailAddress){
        data = new MutableLiveData<>();
        Mauth.sendPasswordResetEmail(EmailAddress).addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                data.setValue(true);
            }else {
                data.setValue(false);
                Toast.Message(application, task.getException().getMessage());
            }
        }).addOnFailureListener(e -> {
            data.setValue(false);
            Toast.Message(application, e.getMessage());
        });
        return data;
    }

}
