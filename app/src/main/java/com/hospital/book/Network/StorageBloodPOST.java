package com.hospital.book.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Utils.Toast;

import java.util.HashMap;

public class StorageBloodPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference BloodBankRef;
    private FirebaseAuth Mauth;

    public StorageBloodPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodBankRef = FirebaseFirestore.getInstance().collection(DataManager.BloodStorage);
    }

    public LiveData<Boolean> BloodStoragePOST(String HospitalLogo, String HospitalName, String Location, long Timestamp, String BloodCategory, String BloodGroup, String TagNumber, String Quantity, String DonorName, String DonorPhoneNumber
    , String BloodDonationDate, String BloodExpiry, String Status){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var map = new HashMap<String, Object>();
            map.put(DataManager.BloodCategory, BloodCategory);
            map.put(DataManager.BloodGroup, BloodGroup);
            map.put(DataManager.TagNumber, TagNumber);
            map.put(DataManager.Quantity, Quantity);
            map.put(DataManager.DonorName, DonorName);
            map.put(DataManager.DonorPhoneNumber, DonorPhoneNumber);
            map.put(DataManager.DonationDate, BloodDonationDate);
            map.put(DataManager.ExpiryDate, BloodExpiry);
            map.put(DataManager.Status, Status);
            map.put(DataManager.UID, FirebaseUser.getUid());
            map.put(DataManager.HospitalLogo, HospitalLogo);
            map.put(DataManager.HospitalName, HospitalName);
            map.put(DataManager.Location, Location);
            map.put(DataManager.DocumentID, Timestamp);
            map.put(DataManager.TimesTamp, Timestamp);

            BloodBankRef.document(String.valueOf(Timestamp)).set(map).addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                }else {
                    data.setValue(false);
                    Toast.Message(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.Message(application, e.getMessage());
            });

        }
        return data;
    }
}
