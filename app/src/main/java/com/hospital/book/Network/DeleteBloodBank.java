package com.hospital.book.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.hospital.book.Data.DataManager;
import com.hospital.book.Utils.Toast;

public class DeleteBloodBank {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference MBloodBankRef;
    private FirebaseAuth Mauth;

    public DeleteBloodBank(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MBloodBankRef = FirebaseFirestore.getInstance().collection(DataManager.BloodBank);
    }

    public LiveData<Boolean> DeleteBloodBank(long DocumentID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            MBloodBankRef.document(String.valueOf(DocumentID)).delete().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                }else {
                    Toast.Message(application, task.getException().getMessage());
                    data.setValue(false);
                }
            }).addOnFailureListener(e -> {
                Toast.Message(application, e.getMessage());
                data.setValue(false);
            });
        }
        return data;
    }
}
