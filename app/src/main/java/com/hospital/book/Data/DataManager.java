package com.hospital.book.Data;

public class DataManager {
    public static final String TypeRegisterAsFriend = "RegisterAsFriend";
    public static final String RegisterDonationBloodImage = "RegisterDonationBloodImage";
    public static final String Organization = "Organization";
    public static final String RegisterDonation = "RegisterDonation";
    public static final String QuantityOfBlood = "QuantityOfBlood";
    public static final String Search = "Search";
    public static final String Location = "Location";
    public static final String RegisterAsFriend = "RegisterAsFriend";
    public static final String Type = "Type";
    public static final String RegisterAsDonor = "RegisterAsDonor";
    public static final String TypeRegisterAsDonor = "RegisterAsDonor";
    public static final String CountryName = "CountryName";
    public static final String RegisterDonorUID = "RegisterDonorUID";
    public static final String NumberOFQuantityPints = "NumberOFQuantityPints";
    public static final String  Notification = "Notification";
    public static final String MessageTopic = "MessageTopic";
    public static final String MessageType = "MessageType";
    public static final String ReceiverUID = "ReceiverUID";
    public static final String BloodRequestAccept = "BloodRequestAccept";

    public static final String MyBloodRequest = "MyBloodRequest";
    public static final String AcceptedQuantity = "AcceptedQuantity";
    public static final String AttendantContactNumber = "AttendantContactNumber";
    public static final String BloodCondition = "BloodCondition";
    public static final String BloodNeedFor = "BloodNeedFor";
    public static final String BloodRequiredDate = "BloodRequiredDate";
    public static final String BloodRequiredTime = "BloodRequiredTime";
    public static final String ExchangePossibility = "ExchangePossibility";
    public static final String PatientName = "PatientName";
    public static final String Accept = "Accept";
    public static final String TransportAvailability = "TransportAvailability";
    public static final String TypeOFHBLevel = "TypeOfHbLevel";
    public static final String TypeOfPatientsCondition = "TypeOfPatientsCondition";
    public static final String BloodRequestID = "BloodRequestID";

    public static final String Province = "Province";
    public static final String Provance = "Provance";
    public static final String OrganizationType = "OrganizationType";
    public static final String Country = "Country";
    public static final String District = "District";
    public static final String BloodBank = "BloodBank";
    public static final String BloodCategory = "BloodCategory";
    public static final String BloodGroup = "BloodGroup";
    public static final String TagNumber = "TagNumber";
    public static final String Quantity = "Quantity";
    public static final String DonorName = "DonorName";
    public static final String DonorPhoneNumber = "DonorPhoneNumber";
    public static final String BloodDonationDate = "BloodDonationDate";
    public static final String BloodExpiry = "BloodExpiry";
    public static final String Status = "Status";
    public static final String UID = "UID";
    public static final String DocumentID = "DocumentID";
    public static final String TimesTamp = "Timestamp";
    public static final String DatePattern = "yyyy-MM-dd";

    public static final String Storage = "Storage";
    public static final String Details = "Details";
    public static final String DonationDate = "DonationDate";
    public static final String ExpiryDate = "ExpiryDate";
    public static final String BloodStorage = "BloodStorage";
    public static final String Expired = "Expired";
    public static final String Data = "Data";
    public static final String Hospital = "Hospital";
    public static final String HospitalLogo = "HospitalLogo";
    public static final String HospitalName = "HospitalName";

    public static final String ProfileImage = "ProfileImage";
    public static final String User = "User";
    public static final String DonorUser = "DonorUser";
    public static final String Call = "Call";
    public static final String Message = "Message";
    public static final String RoshanNumber = "RoshanNumber";

    public static final String MainPhoneNumber = "MainPhoneNumber";
    public static final String EtisalatNumber = "EtisalatNumber";
    public static final String AWCCNumber = "AWCCNumber";
    public static final String MTNNumber = "MTNNumber";
    public static final String WhatsappNumber = "WhatsappNumber";
    public static final String EmailAddress = "EmailAddress";
    public static final String DonateMonth = "DonateMonth";
    public static final String LastDonationDate = "LastDonationDate";
    public static final String EnableMyNumber = "EnableMyNumber";
    public static final String BloodRequest = "BloodRequest";
    public static final String BloodRequestTimestamp = "BloodRequestTimestamp";
    public static final String GivenName = "GivenName";
    public static final String Surname = "Surname";
    public static final String FatherName = "FatherName";
    public static final String Age = "Age";
    public static final String Width = "Width";
    public static final String AreaName = "AreaName";
    public static final String SenderUID = "SenderUID";
    public static final String LastDonationDayCount = "LastDonationDayCount";
    public static final String Timestamp = "Timestamp";
    public static final String DocumentKey = "DocumentKey";


    public static final String APlus = "A+";
    public static final String AMinus = "A-";
    public static final String BPlus = "B+";
    public static final String BMinus = "B-";
    public static final String ABPlus = "AB+";
    public static final String ABMinus = "AB-";
    public static final String OPlus = "O+";
    public static final String OMinus = "O-";
}
