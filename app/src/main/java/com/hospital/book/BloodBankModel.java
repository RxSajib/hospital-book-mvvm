package com.hospital.book;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BloodBankModel implements Serializable {

    private String BloodCategory, BloodGroup, DonorName, DonorPhoneNumber, Quantity, TagNumber, UID, DonationDate, ExpiryDate, Status;
    private long Timestamp, DocumentID;
}
