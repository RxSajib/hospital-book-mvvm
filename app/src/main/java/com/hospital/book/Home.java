package com.hospital.book;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.hospital.book.Network.ViewModel.ViewModel;
import com.hospital.book.Utils.HandleActivity;
import com.hospital.book.databinding.HomeBinding;

public class Home extends AppCompatActivity {

    private HomeBinding binding;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.home);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        InitView();
    }

    private void InitView(){

        binding.BloodDonorBtn.setOnClickListener(view -> {
            HandleActivity.GotoBloodDonor(Home.this);
            Animatoo.INSTANCE.animateSlideLeft(Home.this);
        });

        binding.ProfileBtn.setOnClickListener(view -> {
            HandleActivity.GotoProfile(Home.this);
            Animatoo.INSTANCE.animateSlideLeft(Home.this);
        });

        binding.TelephoneDirectory.setOnClickListener(view -> {
            HandleActivity.GotoTelephoneDirectory(Home.this);
            Animatoo.INSTANCE.animateSlideLeft(Home.this);
        });

        binding.BloodBank.setOnClickListener(view -> {
            HandleActivity.GotoBloodBank(Home.this);
            Animatoo.INSTANCE.animateSlideLeft(Home.this);
        });

        binding.FindDonor.setOnClickListener(view -> {
            HandleActivity.GotoFindDonorFilter(Home.this);
            Animatoo.INSTANCE.animateSlideLeft(Home.this);
        });

        binding.BloodRequest.setOnClickListener(view -> {
            HandleActivity.GotoRequestOFBloodHome(Home.this);
            Animatoo.INSTANCE.animateSlideLeft(Home.this);
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        viewModel.UserExists().observe(this, aBoolean -> {
            if(!aBoolean){
                HandleActivity.GotoSplashScreen(Home.this);
                finish();
            }
        });
    }

}