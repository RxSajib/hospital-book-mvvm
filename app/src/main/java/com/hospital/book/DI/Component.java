package com.hospital.book.DI;

import com.hospital.book.DonorUserDetails;
import com.hospital.book.UI.DetailsOFBloodRequest;
import com.hospital.book.UI.EditBloodBank;
import com.hospital.book.UI.FindDonorsResult;
import com.hospital.book.UI.ForgotPassword;
import com.hospital.book.UI.RegisterDonor;
import com.hospital.book.UI.SignIn;
import com.hospital.book.UI.TelephoneDirectory;

@dagger.Component
public interface Component {

    public void InjectSignIn(SignIn signIn);
    public void InjectForgotPassword(ForgotPassword forgotPassword);
    public void InjectTelephoneDirectory(TelephoneDirectory telephoneDirectory);
    public void InjectEditBloodBank(EditBloodBank editBloodBank);
    public void InjectFindDonorsResult(FindDonorsResult findDonorsResult);
    public void InjectDonorUserDetails(DonorUserDetails donorUserDetails);
    public void InjectRegisterDonor(RegisterDonor registerDonor);

    void InjectDetailsOFBloodRequest(DetailsOFBloodRequest detailsOFBloodRequest);
}
